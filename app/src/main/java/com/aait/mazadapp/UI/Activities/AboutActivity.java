package com.aait.mazadapp.UI.Activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.AboutResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.about)
    TextView about;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.About_the_application));
        search.setVisibility(View.GONE);
        getAbout();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about_app;
    }
    private void getAbout(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAbout(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<AboutResponse>() {
            @Override
            public void onResponse(Call<AboutResponse> call, Response<AboutResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        about.setText(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AboutResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

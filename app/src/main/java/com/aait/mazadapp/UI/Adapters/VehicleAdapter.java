package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.R;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;
import io.fotoapparat.parameter.Flash;

public class VehicleAdapter extends ParentRecyclerAdapter<ListModel> {
    public VehicleAdapter(Context context, List<ListModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        VehicleAdapter.ViewHolder holder = new VehicleAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final VehicleAdapter.ViewHolder viewHolder = (VehicleAdapter.ViewHolder) holder;
        final ListModel listModel = data.get(position);
        viewHolder.vehicle.setText(listModel.getName());
        if (listModel.isChecked()){
            viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimary));
        }else {
            viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorOffWhite));
        }
        viewHolder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });


    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.vehicle)
        TextView vehicle;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }

}

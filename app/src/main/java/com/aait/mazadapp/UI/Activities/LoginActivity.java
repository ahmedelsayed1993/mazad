package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.password)
    EditText password;
    String newToken = null;
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( LoginActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }
    @OnClick(R.id.register)
    void onRegister(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }
    @OnClick(R.id.login)
    void onLogin(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone_number))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))){
            return;
        }else {
            login();
        }

    }
    @OnClick(R.id.forgot_pass)
    void onForgot(){
        startActivity(new Intent(mContext,ForgotPasswordActivity.class));
    }
    @OnClick(R.id.skip)
    void onSkip(){
        mSharedPrefManager.setLoginStatus(false);
        startActivity(new Intent(mContext,MainActivity.class));
    }

    private void login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).login(mLanguagePrefManager.getAppLanguage(),phone.getText().toString(),password.getText().toString(),newToken,"android").enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                Log.e("err",new Gson().toJson(response.body().getData()));
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        mSharedPrefManager.setLoginStatus(true);
                        mSharedPrefManager.setUserData(response.body().getData());
                        startActivity(new Intent(mContext,MainActivity.class));
                        LoginActivity.this.finish();
                    }else if (response.body().getKey().equals("2")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        Intent intent = new Intent(mContext,ActivtateAccountActivity.class);
                        intent.putExtra("user",response.body().getData());
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                Log.e("rrr",new Gson().toJson(t));
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

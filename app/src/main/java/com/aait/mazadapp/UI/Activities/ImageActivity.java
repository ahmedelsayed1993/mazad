package com.aait.mazadapp.UI.Activities;

import android.view.View;
import android.widget.ImageView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;

public class ImageActivity extends ParentActivity {
    @BindView(R.id.search)
    ImageView search;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.image)
    ImageView image;
    String path;
    @Override
    protected void initializeComponents() {
        search.setVisibility(View.GONE);
        path = getIntent().getStringExtra("path");
        Glide.with(mContext).load(path).into(image);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_image;
    }
}

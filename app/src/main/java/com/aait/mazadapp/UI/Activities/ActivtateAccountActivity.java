package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.CheckCodeResponse;
import com.aait.mazadapp.Models.ResedResponse;
import com.aait.mazadapp.Models.UserModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivtateAccountActivity extends ParentActivity {
    @BindView(R.id.code)
    EditText code;
    UserModel userModel;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }

    @Override
    protected void initializeComponents() {
        userModel = (UserModel)getIntent().getSerializableExtra("user");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_activiate_account;
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,code,getString(R.string.activation_code))){
            return;
        }else {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).check(mLanguagePrefManager.getAppLanguage(),userModel.getId(),code.getText().toString()).enqueue(new Callback<CheckCodeResponse>() {
                @Override
                public void onResponse(Call<CheckCodeResponse> call, Response<CheckCodeResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getKey().equals("1")){
                            CommonUtil.makeToast(mContext,response.body().getData());
                            startActivity(new Intent(mContext,LoginActivity.class));
                            ActivtateAccountActivity.this.finish();
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CheckCodeResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }
    }
    @OnClick(R.id.resend)
    void onResend(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).resend(mLanguagePrefManager.getAppLanguage(),userModel.getId()).enqueue(new Callback<ResedResponse>() {
            @Override
            public void onResponse(Call<ResedResponse> call, Response<ResedResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResedResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.Intent;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;

import butterknife.OnClick;

public class BackToHomeActivity extends ParentActivity {
    String categoriesModel;
    String aucationsModel;
    @Override
    protected void initializeComponents() {
        categoriesModel = getIntent().getStringExtra("category");
        aucationsModel = getIntent().getStringExtra("auction");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_back_to_home;
    }
    @OnClick(R.id.back)
    void onBack(){
        Intent intent = new Intent(mContext,AuctionDetailsActivity.class);
        intent.putExtra("category",categoriesModel);

        intent.putExtra("auction",aucationsModel);
        startActivity(intent);
        BackToHomeActivity.this.finish();
    }
}

package com.aait.mazadapp.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Fragments.ChatsFragment;
import com.aait.mazadapp.UI.Fragments.HomeFragment;
import com.aait.mazadapp.UI.Fragments.ProfileFragment;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.etebarian.meowbottomnavigation.MeowBottomNavigation;

import java.util.Stack;

import butterknife.BindView;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MainActivity extends ParentActivity {
    @BindView(R.id.bottom)
    MeowBottomNavigation bottomNavigation;
    protected int selectedTab = 0;
    private Stack<Integer> tabsStack = new Stack<>();

    private FragmentManager fragmentManager;

    private FragmentTransaction transaction;
    HomeFragment mHomeFragment;
    ChatsFragment mChatsFragment;

    ProfileFragment mProfileFragment;


    @Override
    protected void initializeComponents() {
        bottomNavigation.add(new MeowBottomNavigation.Model(1, R.mipmap.home_messages));
        bottomNavigation.add(new MeowBottomNavigation.Model(2, R.mipmap.chat_home));
        bottomNavigation.add(new MeowBottomNavigation.Model(3, R.mipmap.user_profile));
        mHomeFragment = HomeFragment.newInstance();
        mChatsFragment = ChatsFragment.newInstance();
        mProfileFragment = ProfileFragment.newInstance();
        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.home_fragment_container, mHomeFragment);

        transaction.add(R.id.home_fragment_container,mProfileFragment);
        transaction.add(R.id.home_fragment_container,mChatsFragment);
        transaction.commit();
        bottomNavigation.show(1,false);
        showHome(true);
        bottomNavigation.setOnClickMenuListener(new Function1<MeowBottomNavigation.Model, Unit>() {
            @Override
            public Unit invoke(MeowBottomNavigation.Model model) {
                switch (model.getId())
                {
                    case 1:
                        bottomNavigation.show(1,false);
                        showHome(true);
                        break;
                    case 2:
                        bottomNavigation.show(2,false);
                        showMenu(true);
                        break;
                    case 3:
                        bottomNavigation.show(3,false);
                        showOrders(true);
                }
                return null;
            }
        });

        if (mSharedPrefManager.getLoginStatus()){

        }else {
            new VisitorDialog(mContext).show();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }
    private void showHome(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mMoreFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mHomeFragment);
        transaction.commit();
        selectedTab = R.id.home;



    }
    private void showMenu(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mOrdersFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mChatsFragment);
        transaction.commit();




    }
    private void showOrders(final boolean b) {
        if (b) {
            if (selectedTab != 0) {
                tabsStack.push(selectedTab);
            }
        }
        transaction = fragmentManager.beginTransaction();
//        transaction.hide(mHomeFragment);
//        transaction.hide(mMoreFragment);
//        transaction.hide(mFavouriteFragment);
        transaction.replace(R.id.home_fragment_container,mProfileFragment);
        transaction.commit();




    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.UserModel;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewPasswordActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.new_password)
    EditText new_password;
    @BindView(R.id.confirm_password)
    EditText confirm_password;
    UserModel userModel;

    @Override
    protected void initializeComponents() {
        userModel = (UserModel) getIntent().getSerializableExtra("user");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_new_password;
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,code,getString(R.string.Validation_code))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,getString(R.string.new_password))||
        CommonUtil.checkLength(new_password,getString(R.string.password_length),6)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_password,getString(R.string.confirm_password))){
            return;
        }else {
            if (new_password.getText().toString().equals(confirm_password.getText().toString())){
                   update();
            }else {
                confirm_password.setError(getString(R.string.password_not_match));
            }
        }
    }
    private void update(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updatePass(mLanguagePrefManager.getAppLanguage(),userModel.getId(),new_password.getText().toString(),code.getText().toString())
        .enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,LoginActivity.class));
                        NewPasswordActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

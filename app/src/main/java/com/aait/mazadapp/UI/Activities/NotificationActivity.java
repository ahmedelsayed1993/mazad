package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.AucationsResponse;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Models.NotificationModel;
import com.aait.mazadapp.Models.NotificationResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.AuctionsAdapter;
import com.aait.mazadapp.UI.Adapters.NotificationAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends ParentActivity implements OnItemClickListener {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;

    ArrayList<NotificationModel> notificationModels = new ArrayList<>();
    NotificationAdapter notificationAdapter;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.notification));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        notificationAdapter = new NotificationAdapter(mContext,notificationModels,R.layout.recycler_notification);
        notificationAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(notificationAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotification();
            }
        });
        getNotification();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notification;
    }
    private void getNotification(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getNotification(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            notificationAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.delete)
        {

            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).delete(mLanguagePrefManager.getAppLanguage(),notificationModels.get(position).getId(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getKey().equals("1")){

                            notificationModels.remove(notificationModels.get(position));
                            notificationAdapter.notifyDataSetChanged();
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            if (notificationModels.get(position).getAuction_id()==0){

            }else {
                if (notificationModels.get(position).getFinished()==0) {
                    if (notificationModels.get(position).getActive()==1) {
                        Intent intent = new Intent(mContext, AuctionDetailsActivity.class);
                        intent.putExtra("category", notificationModels.get(position).getCategory_key());

                        intent.putExtra("auction", notificationModels.get(position).getAuction_id() + "");
                        startActivity(intent);
                    }else {
                        Intent intent = new Intent(mContext, ComingDetailsActivity.class);
                        intent.putExtra("category", notificationModels.get(position).getCategory_key());

                        intent.putExtra("auction", notificationModels.get(position).getAuction_id() + "");
                        startActivity(intent);
                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.auction_finished));
                }
            }
        }

    }


}

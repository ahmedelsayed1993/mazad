package com.aait.mazadapp.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.CategoriesResponse;
import com.aait.mazadapp.Models.ChatsModel;
import com.aait.mazadapp.Models.ChatsResponse;
import com.aait.mazadapp.Models.MyChatsResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.ChatActivity;
import com.aait.mazadapp.UI.Activities.NotificationActivity;
import com.aait.mazadapp.UI.Adapters.CategoryAdapter;
import com.aait.mazadapp.UI.Adapters.ChatsAdapter;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.io.Serializable;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatsFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ChatsModel> chatsModels = new ArrayList<>();
    ChatsAdapter chatsAdapter;
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    public static ChatsFragment newInstance() {
        Bundle args = new Bundle();
        ChatsFragment fragment = new ChatsFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_chats;
    }

    @Override
    protected void initializeComponents(View view) {
        title.setText(getString(R.string.messages));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        chatsAdapter = new ChatsAdapter(mContext,chatsModels,R.layout.recycler_chats);
        chatsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(chatsAdapter);
        if (mSharedPrefManager.getLoginStatus()) {
            swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getChats();
                }
            });
            getChats();
        }else {

        }

    }

    private void getChats(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getMyChats(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<MyChatsResponse>() {
            @Override
            public void onResponse(Call<MyChatsResponse> call, Response<MyChatsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            chatsAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<MyChatsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext, ChatActivity.class);
        intent.putExtra("room",chatsModels.get(position).getConversation_id()+"");
        intent.putExtra("auction",chatsModels.get(position).getAuction_id()+"");
        intent.putExtra("user",chatsModels.get(position).getUsername());
        intent.putExtra("lastpage",chatsModels.get(position).getLastPage()+"");
        intent.putExtra("reciver" ,chatsModels.get(position).getUser_id()+"");
        startActivity(intent);
    }
}

package com.aait.mazadapp.UI.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.Models.ListResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.VehicleAdapter;
import com.aait.mazadapp.UI.Fragments.CommingFragment;
import com.aait.mazadapp.UI.Views.ListDialog;
import com.aait.mazadapp.UI.Views.PriceDialog;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.reset)
    void onReset(){
         vehicle_id= null;
         brand_id = null;
         city_id = null;
         Advertiser_id = null;
         Model_id = null;
         count_id = null;
         year = null;
         min_price = null;
         max_price = null;
         region_id = null;
         number = null;
         property = null;
         area = null;
    }
    CategoriesModel categoriesModel;
    @BindView(R.id.cars)
    RelativeLayout cars;
    @BindView(R.id.any)
    RelativeLayout any;
    @BindView(R.id.plates)
    RelativeLayout plates;
    @BindView(R.id.estate)
    RelativeLayout estate;
    @BindView(R.id.city)
    LinearLayout city;
    ListDialog listDialog;
    ArrayList<ListModel> cities = new ArrayList<>();
    ArrayList<ListModel> brands = new ArrayList<>();
    ArrayList<ListModel> models = new ArrayList<>();
    @BindView(R.id.vehicles)
    RecyclerView vehicles;
    GridLayoutManager gridLayoutManager;
    ArrayList<ListModel> vehicle_list = new ArrayList<>();
    ArrayList<ListModel> Advertisers = new ArrayList<>();
    ArrayList<ListModel> Count = new ArrayList<>();
    ArrayList<ListModel> years = new ArrayList<>();
    ArrayList<ListModel> Regions = new ArrayList<>();
    ArrayList<ListModel> property_type = new ArrayList<>();
    ArrayList<ListModel> Areas = new ArrayList<>();
    VehicleAdapter vehicleAdapter;
    @BindView(R.id.type)
    LinearLayout type;
    @BindView(R.id.agncey)
    LinearLayout agncey;
    @BindView(R.id.advertiser)
            LinearLayout advertiser;
    @BindView(R.id.region)
            LinearLayout region;
    @BindView(R.id.any_advertiser_text)
    TextView any_advertiser_text;
    @BindView(R.id.any_city_text)
    TextView any_city_text;
    @BindView(R.id.region_text)
    TextView region_text;
    @BindView(R.id.estate_advertiser_text)
    TextView estate_advertiser_text;
    @BindView(R.id.property_text)
    TextView property_text;
    @BindView(R.id.space_text)
    TextView space_text;
    @BindView(R.id.city_text)
    TextView city_text;
    @BindView(R.id.type_text)
            TextView type_text;
    @BindView(R.id.agncey_text)
            TextView agncey_text;
    @BindView(R.id.model_text)
            TextView model_text;
    @BindView(R.id.advertiser_text)
            TextView advertiser_text;
    @BindView(R.id.odometer_text)
            TextView odometer_text;
    @BindView(R.id.make_year_text)
            TextView make_year_text;
    String vehicle_id= null;
    String brand_id = null;
    String city_id = null;
    String Advertiser_id = null;
    String Model_id = null;
    String count_id = null;
    String year = null;
    String min_price = null;
    String max_price = null;
    String region_id = null;
    String number = null;
    String property = null;
    String area = null;
    int selected = 0;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.filter));
        vehicles.setVisibility(View.GONE);
        gridLayoutManager = new GridLayoutManager(mContext,4);
        vehicleAdapter = new VehicleAdapter(mContext,vehicle_list,R.layout.vehicle_recycler);
        vehicleAdapter.setOnItemClickListener(this);
        vehicles.setLayoutManager(gridLayoutManager);
        vehicles.setAdapter(vehicleAdapter);
        property_type.add(new ListModel("land",getString(R.string.land)));
        property_type.add(new ListModel("commercial",getString(R.string.commercial)));
        property_type.add(new ListModel("flat",getString(R.string.flat)));
        property_type.add(new ListModel("villa",getString(R.string.villa)));
        property_type.add(new ListModel("build",getString(R.string.build)));
        property_type.add(new ListModel("store",getString(R.string.store)));
        categoriesModel = (CategoriesModel) getIntent().getSerializableExtra("category");
        if (categoriesModel.getCategory_key().equals("cars")){
            cars.setVisibility(View.VISIBLE);
            any.setVisibility(View.GONE);
            plates.setVisibility(View.GONE);
            estate.setVisibility(View.GONE);
        }else if (categoriesModel.getCategory_key().equals("plates"))
        {
            plates.setVisibility(View.VISIBLE);
            cars.setVisibility(View.GONE);
            any.setVisibility(View.GONE);
            estate.setVisibility(View.GONE);
        }else if (categoriesModel.getCategory_key().equals("estate")){
            plates.setVisibility(View.GONE);
            cars.setVisibility(View.GONE);
            any.setVisibility(View.GONE);
            estate.setVisibility(View.VISIBLE);
        }
        else {
            plates.setVisibility(View.GONE);
            cars.setVisibility(View.GONE);
            estate.setVisibility(View.GONE);
            any.setVisibility(View.VISIBLE);
        }

    }
    @OnClick(R.id.all)
    void onAll(){
        vehicle_id= null;
        brand_id = null;
        city_id = null;
        Advertiser_id = null;
        Model_id = null;
        count_id = null;
        year = null;
        min_price = null;
        max_price = null;
        region_id = null;
        number = null;
        property = null;
        area = null;
    }
    @OnClick(R.id.any_all)
    void onAnyAll(){
        vehicle_id= null;
        brand_id = null;
        city_id = null;
        Advertiser_id = null;
        Model_id = null;
        count_id = null;
        year = null;
        min_price = null;
        max_price = null;
        region_id = null;
        number = null;
        property = null;
        area = null;
    }
    @OnClick(R.id.estate_all)
    void onEstateAll(){
        vehicle_id= null;
        brand_id = null;
        city_id = null;
        Advertiser_id = null;
        Model_id = null;
        count_id = null;
        year = null;
        min_price = null;
        max_price = null;
        region_id = null;
        number = null;
        property = null;
        area = null;
    }
    @OnClick(R.id.plates_all)
    void onPlatesAll(){
        vehicle_id= null;
        brand_id = null;
        city_id = null;
        Advertiser_id = null;
        Model_id = null;
        count_id = null;
        year = null;
        min_price = null;
        max_price = null;
        region_id = null;
        number = null;
        property = null;
        area = null;
    }
    @OnClick(R.id.one)
    void onOne(){
        number = "1";
    }
    @OnClick(R.id.two)
    void onTwo(){
        number = "2";
    }
    @OnClick(R.id.three)
    void onThree(){
        number = "3";
    }
    @OnClick(R.id.four)
    void onFour(){
        number = "4";
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_filter;
    }
    @OnClick(R.id.city)
    void onCity(){
        selected = 0;
        getCities();
    }
    @OnClick(R.id.type)
    void onType(){
        getVehicles();
    }
    @OnClick(R.id.agncey)
    void onAgncey(){
        if (vehicle_id==null){
            CommonUtil.makeToast(mContext,getString(R.string.Select_the_vehicle_type_first));
        }else {
            selected = 1;
            getBrands();
        }
    }
    @OnClick(R.id.model)
    void onModel(){
        selected = 2;
        if (brand_id==null){
             CommonUtil.makeToast(mContext,getString(R.string.Determine_the_vehicle_type_and_agency_first));
        }else {
            getModels();
        }
    }
    @OnClick(R.id.advertiser)
    void onAdvertiser(){
        selected = 3;
        getAdver();
    }
    @OnClick(R.id.odometer)
    void onOdometer(){
        selected = 4;
        getCount();
    }
    @OnClick(R.id.make_year)
    void onYear(){
        selected = 5;
        years.clear();
        for (int i = Calendar.getInstance().get(Calendar.YEAR);i>=1970 ;i--){
            years.add(new ListModel(i+"",i+""));
        }
        listDialog = new ListDialog(mContext,FilterActivity.this,years,getString(R.string.manufacturing_year));
        listDialog.show();


    }
    @OnClick(R.id.property_type)
    void onProperty_type(){
        selected = 7;
        listDialog = new ListDialog(mContext,FilterActivity.this,property_type,getString(R.string.Property_type));
        listDialog.show();
    }
    @OnClick(R.id.price)
    void onPrice(){
        price();
    }
    @OnClick(R.id.estate_price)
    void onEstatePrice(){
        price();
    }
    @OnClick(R.id.plate_price)
    void onPlatePrice(){
       price();
    }
    @OnClick(R.id.any_price)
    void onAnyPrice(){
        price();
    }
    @OnClick(R.id.any_city)
    void onAnyCity(){
        selected = 0;
        getCities();
    }
    @OnClick(R.id.any_advertiser)
    void onAnyAdvertiser(){
        selected = 3;
        getAdver();
    }
    @OnClick(R.id.estate_advertiser)
    void onEstateAdvertiser(){
        selected = 3;
        getAdver();
    }
    @OnClick(R.id.region)
    void onRegion(){
        selected = 6;
       getRegions();
    }
    @OnClick(R.id.space)
    void onSpace(){
        selected = 8;
        getAreas();
    }
    private void price(){
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.dialog_price);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.setCancelable(false);
        EditText from = dialog.findViewById(R.id.from);
        EditText to = dialog.findViewById(R.id.to);
        TextView done = dialog.findViewById(R.id.done);
        TextView cancel = dialog.findViewById(R.id.cancel);
        TextView retrn = dialog.findViewById(R.id.retrn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CommonUtil.checkTextError((AppCompatActivity)mContext,from,getString(R.string.from))||
                        CommonUtil.checkTextError((AppCompatActivity)mContext,to,getString(R.string.to)))
                {
                    return;
                }else {
                    min_price = from.getText().toString();
                    max_price = to.getText().toString();
                    dialog.dismiss();
                }
            }
        });
        retrn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                to.setText("");
                from.setText("");
            }
        });
        dialog.show();
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        cities = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,cities,getString(R.string.City));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getRegions(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getRegions(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Regions = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,Regions,getString(R.string.Region));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getAreas(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAreas(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Areas = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,Areas,getString(R.string.The_space));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getBrands(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getBrands(mLanguagePrefManager.getAppLanguage(),vehicle_id).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        brands = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,brands,getString(R.string.agency));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getModels(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getModel(mLanguagePrefManager.getAppLanguage(),brand_id).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        models = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,models,getString(R.string.Model));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getAdver(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAdver(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId()+"").enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Advertisers = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this,Advertisers,getString(R.string.Advertiser));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getCount(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCount(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId()+"").enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Count = response.body().getData();
                        listDialog = new ListDialog(mContext,FilterActivity.this, Count,getString(R.string.odometer));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getVehicles(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getVehicles(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        vehicle_list = response.body().getData();
                        vehicles.setVisibility(View.VISIBLE);
                        vehicleAdapter.updateAll(vehicle_list);

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId() == R.id.lay){
            if (!vehicle_list.get(position).isChecked()){
                for (int i = 0; i < vehicle_list.size(); i++) {
                    vehicle_list.get(i).setChecked(false);
                }
                vehicle_list.get(position).setChecked(true);
                vehicleAdapter.notifyDataSetChanged();
            }
            vehicle_id = vehicle_list.get(position).getId();
            type_text.setText(vehicle_list.get(position).getName());
        }else if (view.getId() == R.id.tv_row_title){
            listDialog.dismiss();
            if (selected==0){
                 city_id = cities.get(position).getId();
                 city_text.setText(cities.get(position).getName());
                 any_city_text.setText(cities.get(position).getName());

            }else if (selected==1){
                brand_id = brands.get(position).getId();
                agncey_text.setText(brands.get(position).getName());
            }else if (selected == 2){
               Model_id = models.get(position).getId();
               model_text.setText(models.get(position).getName());
            }else if (selected == 3){
                Advertiser_id = Advertisers.get(position).getId();
                advertiser_text.setText(Advertisers.get(position).getName());
                any_advertiser_text.setText(Advertisers.get(position).getName());
                estate_advertiser_text.setText(Advertisers.get(position).getName());
            }else if (selected == 4){
                count_id = Count.get(position).getId();
                odometer_text.setText(Count.get(position).getName());
            }else if (selected == 5){
                year = years.get(position).getId();
                make_year_text.setText(years.get(position).getName());
            }else if (selected==6){
                region_id = Regions.get(position).getId();
                region_text.setText(Regions.get(position).getName());
            } else if (selected == 7){
                property = property_type.get(position).getId();
                property_text.setText(property_type.get(position).getName());
            }else if (selected == 8){
                area = Areas.get(position).getId();
                space_text.setText(Areas.get(position).getName());
            }
        }

    }
    @OnClick(R.id.done)
    void onDone(){
        Intent intentData = new Intent();
        intentData.putExtra("city", city_id);
        intentData.putExtra("brand", brand_id);
        intentData.putExtra("model", Model_id);
        intentData.putExtra("vehicle", vehicle_id);
        intentData.putExtra("advertiser", Advertiser_id);
        intentData.putExtra("count",count_id);
        intentData.putExtra("year",year);
        intentData.putExtra("region",region_id);
        intentData.putExtra("min",min_price);
        intentData.putExtra("max",max_price);
        intentData.putExtra("number",number);
        intentData.putExtra("property",property);
        intentData.putExtra("area",area);
        setResult(RESULT_OK, intentData);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        Intent intentData = new Intent();
//        intentData.putExtra("city", city_id);
////        intentData.putExtra("LOCATION", mResult1);
////        intentData.putExtra("LOC", mResult2);
////        intentData.putExtra(Constant.LocationConstant.LAT, mLat);
////        intentData.putExtra(Constant.LocationConstant.LNG, mLang);
//        setResult(RESULT_OK, intentData);
        finish();
    }
}

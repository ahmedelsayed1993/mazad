package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.aait.mazadapp.Uitls.ProgressRequestBody;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazadapp.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.address)
    TextView address;
    String mAdresse="", mLang=null, mLat = null, mAddress="",Address = "";
    @BindView(R.id.nation)
    TextView nation;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.phone)
            EditText phone;
    @BindView(R.id.email)
            EditText email;
    @BindView(R.id.national_id)
            EditText national_id;
    @BindView(R.id.password)
            EditText password;
    @BindView(R.id.confirm_password)
            EditText confirm_password;
    @BindView(R.id.terms)
    CheckBox terms;
    @BindView(R.id.id_image)
            TextView id_image;
    String gender = "";
    String newToken = null;
    private String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( RegisterActivity.this,  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }
@OnClick(R.id.login)
void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
}
    @OnClick(R.id.register)
    void onRegister(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone_number))||
        CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
        CommonUtil.checkTextError(address,getString(R.string.address))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.email))||
        !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
        CommonUtil.checkTextError(nation,getString(R.string.nation))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,national_id,getString(R.string.national_id))||
        CommonUtil.checkLength(national_id,getString(R.string.id_length),10)||
                CommonUtil.checkTextError(id_image,getString(R.string.id_image))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.password))||
        CommonUtil.checkLength(password,getString(R.string.password_length),6)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_password,getString(R.string.confirm_password))){
            return;
        }else {
            if (password.getText().toString().equals(confirm_password.getText().toString())){
                if (terms.isChecked()) {
                    if (ImageBasePath!=null) {
                        register(ImageBasePath);
                    }else {
                        CommonUtil.makeToast(mContext,getString(R.string.id_image));
                    }
                }else {
                    CommonUtil.makeToast(mContext,getString(R.string.check_terms));
                }
            }else {
                confirm_password.setError(getString(R.string.password_not_match));
            }
        }

    }

    @OnClick(R.id.id_image)
    void onImage(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }
    @OnClick(R.id.Terms)
    void onTerms(){
        startActivity(new Intent(mContext,TermsActivity.class));
    }
    @OnClick(R.id.skip)
    void onSkip(){
        mSharedPrefManager.setLoginStatus(false);
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @OnClick(R.id.address)
    void onAddress(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    @OnClick(R.id.nation)
    void onNation(){
        PopupMenu popupMenu = new PopupMenu(mContext, nation);

        popupMenu.inflate(R.menu.gender);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.male:
                        nation.setText(getString(R.string.male));
                        gender = "male";
                        return true;
                    case R.id.female:
                        nation.setText(getString(R.string.female));
                        gender = "female";
                        return true;
                }
                return false;
            }
        });

        popupMenu.show();
    }

    private void register(String path){
        showProgressDialog(getString(R.string.please_wait));

        File ImageFile = new File(path);
        Log.e("img",ImageFile.getName());
        RequestBody fileBody =  RequestBody.create(MediaType.parse("images/*"), ImageFile);
        Log.e("part",fileBody.toString());
       // ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        MultipartBody.Part filePart = MultipartBody.Part.createFormData("copy_residence", "image", fileBody);
        Log.e("file",filePart.toString());
        RetroWeb.getClient().create(ServiceApi.class).register(mLanguagePrefManager.getAppLanguage(),name.getText().toString(),phone.getText().toString()
                ,mLat,mLang,address.getText().toString()
        ,email.getText().toString(),gender,national_id.getText().toString(),newToken,"android"
                ,password.getText().toString(),filePart)
         .enqueue(new Callback<UserResponse>() {
             @Override
             public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                 hideProgressDialog();
                 if (response.isSuccessful()){
                     if (response.body().getKey().equals("1")){
                        Intent intent = new Intent(mContext,ActivtateAccountActivity.class);
                        intent.putExtra("user",response.body().getData());
                        startActivity(intent);
                     }else {
                         CommonUtil.makeToast(mContext,response.body().getMsg());
                     }
                 }
             }

             @Override
             public void onFailure(Call<UserResponse> call, Throwable t) {
                 CommonUtil.handleException(mContext,t);
                 t.printStackTrace();
                 hideProgressDialog();

             }
         });    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("yyyy",requestCode+"");
        Log.e("xxx",resultCode+"");
            if (requestCode == 100) {
                if(data!=null) {
                    Log.e("ddd",new Gson().toJson(data.getStringArrayListExtra(Pix.IMAGE_RESULTS)));
                    returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                    ImageBasePath = returnValue.get(0);
                    Log.e("image", ImageBasePath);
                    // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                    id_image.setText(getString(R.string.image_attached));
                    if (ImageBasePath != null) {

                    }
                }else {
                    Log.e("data","error");
                }

            } else if (requestCode == Constant.RequestCode.GET_LOCATION) {
                  if (data!=null) {
                      Log.e("ddd",new Gson().toJson(data.getData()));
                      mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                      mAddress = data.getStringExtra("LOCATION");
                      Address = data.getStringExtra("LOC");
                      mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                      mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                      CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
                      if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                          address.setText(mAddress);
                      } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                          address.setText(mAdresse);
                      }
                  }
            }


    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}

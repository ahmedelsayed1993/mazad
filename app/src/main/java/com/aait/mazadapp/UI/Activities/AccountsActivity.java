package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.SubscribeTapAdapter;
import com.aait.mazadapp.UI.Adapters.WalletTapAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.OnClick;

public class AccountsActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.orders)
    TabLayout myOrdersTab;
    @BindView(R.id.ordersViewPager)
    ViewPager myOrdersViewPager;


    private WalletTapAdapter mAdapter;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.search)
    ImageView search;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.recharge_the_balance));
        search.setVisibility(View.GONE);
        mAdapter = new WalletTapAdapter(mContext,getSupportFragmentManager());
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_accounts;
    }
}

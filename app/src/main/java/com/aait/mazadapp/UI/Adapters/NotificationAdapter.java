package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.NotificationModel;
import com.aait.mazadapp.Models.QuestionModel;
import com.aait.mazadapp.R;
import com.daimajia.swipe.SwipeLayout;

import java.util.List;

import butterknife.BindView;
import io.fotoapparat.parameter.Flash;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {
    public NotificationAdapter(Context context, List<NotificationModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        NotificationAdapter.ViewHolder holder = new NotificationAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final NotificationAdapter.ViewHolder viewHolder = (NotificationAdapter.ViewHolder) holder;
        final NotificationModel notificationModel = data.get(position);
        viewHolder.text.setText(notificationModel.getContent());
        viewHolder.time.setText(notificationModel.getCreated());


        //dari kiri

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.time)
        TextView time;

        @BindView(R.id.delete)
                ImageView delete;








        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

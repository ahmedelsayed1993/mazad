package com.aait.mazadapp.UI.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.AucationsResponse;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.CategoriesResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.AuctionDetailsActivity;
import com.aait.mazadapp.UI.Activities.FilterActivity;
import com.aait.mazadapp.UI.Activities.ImageActivity;
import com.aait.mazadapp.UI.Activities.SortActivity;
import com.aait.mazadapp.UI.Adapters.AuctionsAdapter;
import com.aait.mazadapp.UI.Adapters.CategoryAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActiveFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    ArrayList<AucationsModel> aucationsModels = new ArrayList<>();
    AuctionsAdapter auctionsAdapter;
    CategoriesModel categoriesModel;
    @BindView(R.id.menu_image)
    ImageView menu_image;
    @BindView(R.id.menu_text)
    TextView menu_text;
    String city = null;
    String min = null;
    String max = null;
    String number = null;
     String brand = null;
     String model = null;
     String vehicale = null;
     String advertiser = null;
     String count = null;
     String year = null;
     String region = null;
     String property = null;
     String area = null;
    String special=null;
    String popular = null;
    String completion = null;
    String day = null;
    String three_days = null;
    String week = null;
    String city_id =null;


    public static ActiveFragment newInstance(CategoriesModel categoriesModel) {
        Bundle args = new Bundle();
        ActiveFragment fragment = new ActiveFragment();
        args.putSerializable("category",categoriesModel);
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_active;
    }

    @Override
    protected void initializeComponents(View view) {
        Bundle bundle = this.getArguments();
        categoriesModel = (CategoriesModel) bundle.getSerializable("category");
        Log.e("CAT",new Gson().toJson(categoriesModel));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        auctionsAdapter = new AuctionsAdapter(mContext,aucationsModels,R.layout.recycler_aucations);
        auctionsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(auctionsAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getAuctions();
            }
        });
        getAuctions();

    }
    @OnClick(R.id.menu)
    void onMenu(){
        if (menu_text.getText().toString().equals(getString(R.string.menu))) {
            menu_text.setText(getString(R.string.show));
            menu_image.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_menu_wd));
            gridLayoutManager = new GridLayoutManager(mContext, 2);
            auctionsAdapter = new AuctionsAdapter(mContext, aucationsModels, R.layout.recycler_auction_menu);
            auctionsAdapter.setOnItemClickListener(this);
            rvRecycle.setLayoutManager(gridLayoutManager);
            rvRecycle.setAdapter(auctionsAdapter);
            swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getAuctions();
                }
            });
            getAuctions();
        }else if (menu_text.getText().toString().equals(getString(R.string.show))){
            menu_text.setText(getString(R.string.menu));
            menu_image.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_menu));
            linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
            auctionsAdapter = new AuctionsAdapter(mContext,aucationsModels,R.layout.recycler_aucations);
            auctionsAdapter.setOnItemClickListener(this);
            rvRecycle.setLayoutManager(linearLayoutManager);
            rvRecycle.setAdapter(auctionsAdapter);
            swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
            swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getAuctions();
                }
            });
            getAuctions();

        }
    }
    @OnClick(R.id.filter)
    void onFilter(){
        Intent intent = new Intent(mContext,FilterActivity.class);
        intent.putExtra("category",categoriesModel);
        startActivityForResult(intent,1);
    }
    @OnClick(R.id.sort)
    void onSort(){
        Intent intent = new Intent(mContext,SortActivity.class);
        intent.putExtra("category",categoriesModel);
        startActivityForResult(intent,2);
    }
    private void getAuctions(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getAuctions(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId(),1).enqueue(new Callback<AucationsResponse>() {
            @Override
            public void onResponse(Call<AucationsResponse> call, Response<AucationsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            aucationsModels = response.body().getData();
                            linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                            auctionsAdapter = new AuctionsAdapter(mContext,response.body().getData(),R.layout.recycler_aucations);
                            auctionsAdapter.setOnItemClickListener(ActiveFragment.this);
                            rvRecycle.setLayoutManager(linearLayoutManager);
                            rvRecycle.setAdapter(auctionsAdapter);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AucationsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.image){
            Intent intent = new Intent(mContext, ImageActivity.class);
            intent.putExtra("path",aucationsModels.get(position).getImage());
            startActivity(intent);
        }else {
            Intent intent = new Intent(mContext, AuctionDetailsActivity.class);
            intent.putExtra("category", aucationsModels.get(position).getCategory());
            Log.e("id", aucationsModels.get(position).getId() + "");
            intent.putExtra("auction", aucationsModels.get(position).getId() + "");
            startActivity(intent);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==1){
            if (data!=null) {
                city = data.getStringExtra("city");
                max = data.getStringExtra("max");
                min = data.getStringExtra("min");
                number = data.getStringExtra("number");
                brand = data.getStringExtra("brand");
                model = data.getStringExtra("model");
                vehicale = data.getStringExtra("vehicle");
                advertiser = data.getStringExtra("advertiser");
                count = data.getStringExtra("count");
                year = data.getStringExtra("year");
                region = data.getStringExtra("region");
                property = data.getStringExtra("property");
                area = data.getStringExtra("area");
//                Log.e("city", city);
//                Log.e("max",max);
//                Log.e("min",min);
//                Log.e("number",number);
                filter();

            }
        }else if (requestCode == 2){
            if (data!=null){
                city_id = data.getStringExtra("city_id");
                special = data.getStringExtra("special");
                popular = data.getStringExtra("popular");
                completion = data.getStringExtra("completion");
                day = data.getStringExtra("day");
                three_days = data.getStringExtra("three_day");

                week = data.getStringExtra("week");
                sort();
            }
        }
    }
    private void filter(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).filter(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId(),region,city
        ,number,property,area,vehicale,brand,model,year,min,max,advertiser,count,1).enqueue(new Callback<AucationsResponse>() {
            @Override
            public void onResponse(Call<AucationsResponse> call, Response<AucationsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            aucationsModels = response.body().getData();
                            linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                            auctionsAdapter = new AuctionsAdapter(mContext,response.body().getData(),R.layout.recycler_aucations);
                            auctionsAdapter.setOnItemClickListener(ActiveFragment.this);
                            rvRecycle.setLayoutManager(linearLayoutManager);
                            rvRecycle.setAdapter(auctionsAdapter);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AucationsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }
    private void sort(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).sort(mLanguagePrefManager.getAppLanguage(),categoriesModel.getId(),city_id,special,popular,completion,day,three_days,week).enqueue(new Callback<AucationsResponse>() {
            @Override
            public void onResponse(Call<AucationsResponse> call, Response<AucationsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            aucationsModels = response.body().getData();
                            linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                            auctionsAdapter = new AuctionsAdapter(mContext,response.body().getData(),R.layout.recycler_aucations);
                            auctionsAdapter.setOnItemClickListener(ActiveFragment.this);
                            rvRecycle.setLayoutManager(linearLayoutManager);
                            rvRecycle.setAdapter(auctionsAdapter);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AucationsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }
}

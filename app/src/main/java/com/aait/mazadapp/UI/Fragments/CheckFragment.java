package com.aait.mazadapp.UI.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Gps.GPSTracker;
import com.aait.mazadapp.Gps.GpsTrakerListener;
import com.aait.mazadapp.Models.CheckResponse;
import com.aait.mazadapp.Models.CheckModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.ChecksAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.DialogUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckFragment extends BaseFragment implements GoogleMap.OnMarkerClickListener, OnMapReadyCallback, GpsTrakerListener {
    @BindView(R.id.map)
    MapView mapView;
    String mResult;
    GoogleMap googleMap;
    Marker myMarker , shopMarker;
    Geocoder geocoder;
    GPSTracker gps;
    boolean startTracker = false;
    public String mLang= "", mLat="";
    MarkerOptions markerOptions;
    private AlertDialog mAlertDialog;
    HashMap<Marker, CheckModel> hmap = new HashMap<>();
    @BindView(R.id.times)
    RecyclerView times;
    ArrayList<CheckModel> checkModels = new ArrayList<>();
    ChecksAdapter checksAdapter;
    LinearLayoutManager linearLayoutManager;
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_check;
    }

    @Override
    protected void initializeComponents(View view) {
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        checksAdapter = new ChecksAdapter(mContext, checkModels,R.layout.recycler_check);
        times.setLayoutManager(linearLayoutManager);
        times.setAdapter(checksAdapter);

    }
    private void getChecks(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).check(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CheckResponse>() {
            @Override
            public void onResponse(Call<CheckResponse> call, Response<CheckResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (!response.body().getData().isEmpty()){
                            Log.e("checks",new Gson().toJson(response.body().getData()));

                            for ( int i = 0;i<response.body().getData().size();i++) {
                                addShop(response.body().getData().get(i));

                            }
                            checksAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CheckResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
        showProgressDialog(getString(R.string.please_wait));
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
//        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this);
        getLocationWithPermission();
    }
    public void putMapMarker(Double lat, Double log) {
        LatLng latLng = new LatLng(lat, log);
        myMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_red)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
// kkjgj

    }
    public void putMapMarker1(Double lat, Double log) {
        LatLng latLng = new LatLng(lat, log);
        myMarker = googleMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("موقعى")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_red)));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f));
// kkjgj

    }
    void addShop(CheckModel chatsModel ){

        markerOptions =
                new MarkerOptions().position(new LatLng(Float.parseFloat(chatsModel.getLat()), Float.parseFloat(chatsModel.getLng())))
                        .title(chatsModel.getBranch())
                        .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_red));
        shopMarker = googleMap.addMarker(markerOptions);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(chatsModel.getLat()), Double.parseDouble(chatsModel.getLng())), 12f));
        hmap.put(shopMarker,chatsModel);

    }

    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getActivity(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(getActivity(),
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()){
                        Toast.makeText(getActivity(), getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
                    else{mResult = addresses.get(0).getAddressLine(0);

                    }

                } catch (IOException e) {}
                googleMap.clear();
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                getChecks();

            }
        }
    }

}

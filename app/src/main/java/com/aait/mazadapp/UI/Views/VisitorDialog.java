package com.aait.mazadapp.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;

import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.LoginActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class VisitorDialog extends Dialog {
    Context mContext;

    public VisitorDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_visitor);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);


        initializeComponents();
    }
    private void initializeComponents() {


    }
    @OnClick(R.id.contin)
    void onCon(){
        VisitorDialog.this.dismiss();
    }
    @OnClick(R.id.login)
    void onLogin(){
        mContext.startActivity(new Intent(mContext, LoginActivity.class));
        VisitorDialog.this.dismiss();
    }

}

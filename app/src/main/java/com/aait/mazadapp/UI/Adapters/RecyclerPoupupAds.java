package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.aait.mazadapp.Models.ImageModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.ImageActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

public class RecyclerPoupupAds extends PagerAdapter {

    Context context;
    List<ImageModel> data;
    // private ImageView.ScaleType scaleType;
    private LayoutInflater layoutInflater;

    public RecyclerPoupupAds(Context context, List<ImageModel> data) {
        this.context = context;

        this.data=data;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View imageLayout = layoutInflater.inflate(R.layout.recycler_image,null);
        // assert imageLayout != null;
        final ImageView imageView = imageLayout.findViewById(R.id.food_img);

        Glide.with(context).load(data.get(position).getImage()).apply(new RequestOptions().fitCenter().override(500,200)).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ImageActivity.class);
                intent.putExtra("path",data.get(position).getImage());
                context.startActivity(intent);

            }
        });
        ViewPager vp =(ViewPager) container;
        vp.addView(imageLayout,0);
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}

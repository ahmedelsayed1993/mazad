package com.aait.mazadapp.UI.Activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.CallUsResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends ParentActivity {
    @BindView(R.id.search)
    ImageView search;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.chat)
    TextView chat;
    @BindView(R.id.whats)
    TextView whats;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.call_us));
        search.setVisibility(View.GONE);
        getCall();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_us;
    }

    private void getCall(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).callUs(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CallUsResponse>() {
            @Override
            public void onResponse(Call<CallUsResponse> call, Response<CallUsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        email.setText(response.body().getData().getEmail());
                        phone.setText(response.body().getData().getPhone());
                        chat.setText(response.body().getData().getChat());
                        whats.setText(response.body().getData().getWhatsapp());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CallUsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

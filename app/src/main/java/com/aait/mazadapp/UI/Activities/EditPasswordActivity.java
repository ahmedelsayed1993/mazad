package com.aait.mazadapp.UI.Activities;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPasswordActivity extends ParentActivity {

    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.old_password)
    EditText old_password;
    @BindView(R.id.new_password)
    EditText new_password;
    @BindView(R.id.confirm_new)
    EditText confirm_new;
    @Override
    protected void initializeComponents() {
        search.setVisibility(View.GONE);
        title.setText(getString(R.string.edit_password));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_password;
    }
    @OnClick(R.id.save)
    void onSave(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,old_password,mContext.getString(R.string.old_password))||
                CommonUtil.checkLength(old_password,mContext.getString(R.string.password_length),6)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,new_password,mContext.getString(R.string.new_password))||
                CommonUtil.checkLength(new_password,mContext.getString(R.string.password_length),6)||
                CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new,mContext.getString(R.string.confirm_new_password))){
            return;
        }else {
            if (new_password.getText().toString().equals(confirm_new.getText().toString())){
                resetPassword();
            }else {
                CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
            }
        }
    }
    private void resetPassword(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).resetPass(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),old_password.getText().toString(),new_password.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}

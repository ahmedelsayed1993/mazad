package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;

import butterknife.BindView;
import butterknife.OnClick;

public class LanguageActivity extends ParentActivity {
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.lang_lay)
    LinearLayout lang_lay;
    @BindView(R.id.down)
    ImageView down;
    @BindView(R.id.lang)
    RadioGroup lang;
    @BindView(R.id.arabic)
    RadioButton arabic;
    @BindView(R.id.english)
    RadioButton english;
    @Override
    protected void initializeComponents() {
        search.setVisibility(View.GONE);
        title.setText(getString(R.string.The_language));
        if (mLanguagePrefManager.getAppLanguage().equals("ar")){
            arabic.setChecked(true);
        }else {
            english.setChecked(true);
        }

    }
    @OnClick(R.id.lang_lay)
    void onLay(){
        if (lang.getVisibility()==View.VISIBLE){
            lang.setVisibility(View.GONE);
            down.setRotation(0);
        }else {
            lang.setVisibility(View.VISIBLE);
            down.setRotation(180);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_language;
    }
    @OnClick(R.id.arabic)
    void OnArabic(){
      mLanguagePrefManager.setAppLanguage("ar");
      startActivity(new Intent(mContext,SplashActivity.class));
      LanguageActivity.this.finish();
    }
    @OnClick(R.id.english)
    void onEnglish(){
        mLanguagePrefManager.setAppLanguage("en");
        startActivity(new Intent(mContext,SplashActivity.class));
        LanguageActivity.this.finish();
    }
}

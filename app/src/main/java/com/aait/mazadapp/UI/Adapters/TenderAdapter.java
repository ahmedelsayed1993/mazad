package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.TendersModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class TenderAdapter extends ParentRecyclerAdapter<TendersModel> {
    public TenderAdapter(Context context, List<TendersModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        TenderAdapter.ViewHolder holder = new TenderAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final TenderAdapter.ViewHolder viewHolder = (TenderAdapter.ViewHolder) holder;
        final TendersModel tendersModel = data.get(position);
        Glide.with(mcontext).load(tendersModel.getAvatar()).into(viewHolder.image);
    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.image)
        CircleImageView image;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }

}

package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.BanksModel;
import com.aait.mazadapp.R;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class BanksAdapter extends ParentRecyclerAdapter<BanksModel> {
    public BanksAdapter(Context context, List<BanksModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        BanksAdapter.ViewHolder holder = new BanksAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final BanksAdapter.ViewHolder viewHolder = (BanksAdapter.ViewHolder) holder;
        final BanksModel banksModel = data.get(position);
        viewHolder.account_name.setText(banksModel.getAccount_name());
        viewHolder.bank_name.setText(banksModel.getBank_name());
        viewHolder.account_number.setText(banksModel.getAccount_number());
        viewHolder.iban.setText(banksModel.getIban_number());

    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.iban)
        TextView iban;
        @BindView(R.id.account_number)
        TextView account_number;
        @BindView(R.id.account_name)
        TextView account_name;
        @BindView(R.id.bank_name)
        TextView bank_name;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

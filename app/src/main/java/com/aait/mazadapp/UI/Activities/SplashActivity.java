package com.aait.mazadapp.UI.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;
import com.rbddevs.splashy.Splashy;

import butterknife.BindView;

public class SplashActivity extends ParentActivity {
    @BindView(R.id.splash_lay)
    LinearLayout splash_lay;
    Animation fade;
    @BindView(R.id.image)
    ImageView image;

    @Override
    protected void initializeComponents() {
//        Splashy splashy = new Splashy(this);
////        splashy.setLogo(R.mipmap.logo);
////        splashy.setTime(5000);
////        splashy.setTitle("");
////        splashy.setBackgroundColor(R.color.colorAccent);
//       splashy. setTitle("").setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent)).setLogo(R.mipmap.logo)
//                .setTitleColor("")
//                .setSubTitle("")
//                .setFullScreen(true)
//                .setTime(5000)
//        .show();
//        if (mSharedPrefManager.getLoginStatus()) {
//                    startActivity(new Intent(mContext, MainActivity.class));
//                    SplashActivity.this.finish();
//                }
//        else {
//                    startActivity(new Intent(mContext,LoginActivity.class));
//                    SplashActivity.this.finish();
//        }


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }
    protected void onResume() {
        super.onResume();
        fade = AnimationUtils.loadAnimation(this, R.anim.enter_from_left);
        image.clearAnimation();
        image.startAnimation(fade);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                if (mSharedPrefManager.getLoginStatus()) {
                    startActivity(new Intent(mContext, MainActivity.class));
                    SplashActivity.this.finish();
                }else {
                    startActivity(new Intent(mContext,LoginActivity.class));
                    SplashActivity.this.finish();
                }

            }

            @Override
            public void onAnimationRepeat(final Animation animation) {
            }
        });
    }
}

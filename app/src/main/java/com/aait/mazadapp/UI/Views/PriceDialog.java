package com.aait.mazadapp.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Models.AuctionDetailsModel;
import com.aait.mazadapp.Pereferences.LanguagePrefManager;
import com.aait.mazadapp.Pereferences.SharedPrefManager;
import com.aait.mazadapp.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PriceDialog extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    AuctionDetailsModel auctionDetailsModel;
    @BindView(R.id.from)
    EditText from;
    @BindView(R.id.to)
    EditText to;
    public PriceDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_price);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);

        setCancelable(false);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
}

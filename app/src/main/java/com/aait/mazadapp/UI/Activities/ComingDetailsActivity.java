package com.aait.mazadapp.UI.Activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Gps.GPSTracker;
import com.aait.mazadapp.Gps.GpsTrakerListener;
import com.aait.mazadapp.Models.AuctionDetailsModel;
import com.aait.mazadapp.Models.AuctionDetailsResponse;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.ImageModel;
import com.aait.mazadapp.Models.LikeResponse;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.Models.RoomIDResponse;
import com.aait.mazadapp.Models.SubScripResponse;
import com.aait.mazadapp.Models.TendersModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.DetailsAdapter;
import com.aait.mazadapp.UI.Adapters.RecyclerPoupupAds;
import com.aait.mazadapp.UI.Adapters.TenderAdapter;
import com.aait.mazadapp.UI.Views.SubscribeDialog;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ComingDetailsActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,GoogleMap.OnMarkerClickListener, GpsTrakerListener {
    GoogleMap googleMap;
    Geocoder geocoder;
    GPSTracker gps;

    public String mLang, mLat;
    boolean startTracker = false;
    private AlertDialog mAlertDialog;
    @BindView(R.id.three_d)
    CardView three_d;
    Marker myMarker;
    String  aucationsModel;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.date)
    TextView date;

    @BindView(R.id.lowest_price)
    TextView lowest_price;

    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.map)
    MapView map;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    ArrayList<ListModel> listModels = new ArrayList<>();
    ArrayList<ImageModel> imageModels = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerPoupupAds recyclerPoupupAds;
    @BindView(R.id.lay_no_internet)
    RelativeLayout lay_no_internet;

    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.like)
    ImageView like;

    LinearLayoutManager linearLayoutManager;
    AuctionDetailsModel auctionDetailsModel;
    String  categoriesModel;

    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.advertiser_name)
    TextView advertiser_name;
    @BindView(R.id.advertiser_phone)
    TextView advertiser_phone;
    @BindView(R.id.details)
    RecyclerView details;
    LinearLayoutManager linearLayoutManager1;
    DetailsAdapter detailsAdapter;
    @BindView(R.id.dots_indicator)
    SpringDotsIndicator indicator;
    @BindView(R.id.higry)
    TextView higry;
    @Override
    protected void initializeComponents() {
        aucationsModel =  getIntent().getStringExtra("auction");
        categoriesModel =  getIntent().getStringExtra("category");

        map.onCreate(mSavedInstanceState);
        map.onResume();
        map.getMapAsync(this);

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        getLocationWithPermission();
        advertiser_phone.setVisibility(View.GONE);
        if (categoriesModel.equals("plates")){
            three_d.setVisibility(View.GONE);
        }else {
            three_d.setVisibility(View.VISIBLE);
        }

         if (mSharedPrefManager.getLoginStatus()) {
             getAuction(mSharedPrefManager.getUserData().getId()+"");
         }else {
             getAuction(null);
         }

    }
    void setData(AuctionDetailsModel auctionDetailsModel){
        imageModels = auctionDetailsModel.getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(mContext,imageModels);
        viewPager.setAdapter(recyclerPoupupAds);

        indicator.setViewPager(viewPager);


        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){ currentPage = 0; }
                viewPager.setCurrentItem(currentPage,true);
                currentPage++;

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);


            }
        }, 2500,2500);
        if (auctionDetailsModel.getIsLike()==1){
            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_eye));
        }else {
            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.nouneye));
        }
        higry.setText(auctionDetailsModel.getEnd_date_hijri());
        if (auctionDetailsModel.getImage_3D().trim().equals("")){
            three_d.setVisibility(View.GONE);
        }else {
            three_d.setVisibility(View.VISIBLE);
        }
        listModels.add(new ListModel(getString(R.string.Announcement_No),auctionDetailsModel.getId()+""));
        if (auctionDetailsModel.getBrand_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Car_brand),auctionDetailsModel.getBrand_id()));
        }
        if (auctionDetailsModel.getCount_distances().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.odometer),auctionDetailsModel.getCount_distances()));
        }
        if (auctionDetailsModel.getModel_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.car_model),auctionDetailsModel.getModel_id()));
        }
        if (auctionDetailsModel.getYear_make().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.manufacturing_year),auctionDetailsModel.getYear_make()));
        }
        if (auctionDetailsModel.getVehicle_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Engine_type),auctionDetailsModel.getVehicle_id()));
        }
        if (auctionDetailsModel.getCity_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.City),auctionDetailsModel.getCity_id()));
        }
        if (auctionDetailsModel.getRegion_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Region),auctionDetailsModel.getRegion_id()));
        }
        if (auctionDetailsModel.getProperty_type().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Property_type),auctionDetailsModel.getProperty_type()));
        }
        if (auctionDetailsModel.getProperty_area().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.The_space),auctionDetailsModel.getProperty_area()));
        }
        if (auctionDetailsModel.getCount_plate_numbers().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.numbers_count),auctionDetailsModel.getCount_plate_numbers()));
        }
        if (auctionDetailsModel.getPlate_number().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.plate_number),auctionDetailsModel.getPlate_number()));
        }
        Log.e("details",new Gson().toJson(listModels));
        name.setText(auctionDetailsModel.getTitle());
        date.setText(auctionDetailsModel.getStart_date());

        lowest_price.setText(auctionDetailsModel.getStarting_price()+" "+getString(R.string.RS));

        description.setText(auctionDetailsModel.getDescription());
        Glide.with(mContext).load(auctionDetailsModel.getAdvertiser_avatar()).into(image);
        advertiser_name.setText(auctionDetailsModel.getAdvertiser_name());
        advertiser_phone.setText(auctionDetailsModel.getAdvertiser_phone());
        linearLayoutManager1 = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        detailsAdapter = new DetailsAdapter(mContext,listModels,R.layout.recycler_details);
        details.setLayoutManager(linearLayoutManager1);
        details.setAdapter(detailsAdapter);
        putMapMarker(Double.parseDouble(auctionDetailsModel.getLat()),Double.parseDouble(auctionDetailsModel.getLng()));


        putMapMarker(Double.parseDouble(auctionDetailsModel.getLat()),Double.parseDouble(auctionDetailsModel.getLng()));

    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_coming_details;
    }
    private void getAuction(String id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAuction(mLanguagePrefManager.getAppLanguage(), Integer.parseInt(aucationsModel),id).enqueue(new Callback<AuctionDetailsResponse>() {
            @Override
            public void onResponse(Call<AuctionDetailsResponse> call, Response<AuctionDetailsResponse> response) {
                hideProgressDialog();
                lay_no_internet.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        auctionDetailsModel = response.body().getData();
                        Log.e("response",new Gson().toJson(response.body().getData()));
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuctionDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                lay_no_internet.setVisibility(View.VISIBLE);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    @OnClick(R.id.three_d)
    void onThree(){
        Log.e("image",auctionDetailsModel.getImage_3D());
        if (auctionDetailsModel.getImage_3D().trim().isEmpty()){

        }else {
            Intent intent = new Intent(mContext, ThreeDimActivity.class);
            intent.putExtra("three", auctionDetailsModel.getImage_3D());
            intent.putExtra("lang", mLanguagePrefManager.getAppLanguage());
            startActivity(intent);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
//        getAuction();
        getLocationWithPermission();
    }
//    @Override
//    public void onTrackerSuccess(Double lat, Double log) {
//        Log.e("Direction", "Direction Success");
//        // dismiss traker dialog
//        if (startTracker) {
//            if (lat != 0.0 && log != 0.0) {
//                hideProgressDialog();
//                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
//                //putMapMarker(lat, log);
//            }
//        }
//    }
//
//    @Override
//    public void onStartTracker() {
//
//    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {


                //LatLngBound will cover all your marker on Google Maps





            }
        });
    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getApplicationContext(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
//                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
//            getCurrentLocation();
        }

    }
    public void putMapMarker(Double lat, Double log) {
        if (googleMap!=null) {
            LatLng latLng = new LatLng(lat, log);

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
            myMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("موقعى")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_red)));
        }

    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                //putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @OnClick(R.id.message)
    void onMessage(){
        if (mSharedPrefManager.getLoginStatus()) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).getRoom(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getId(), auctionDetailsModel.getId()).enqueue(new Callback<RoomIDResponse>() {
                @Override
                public void onResponse(Call<RoomIDResponse> call, Response<RoomIDResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {

                            Intent intent = new Intent(mContext, ChatActivity.class);
                            intent.putExtra("room", response.body().getData().getConversation_id() + "");
                            intent.putExtra("auction", auctionDetailsModel.getId() + "");
                            intent.putExtra("user", auctionDetailsModel.getAdvertiser_name());
                            intent.putExtra("lastpage", response.body().getData().getLastPage() + "");
                            intent.putExtra("reciver", auctionDetailsModel.getAdvertiser_id() + "");
                            startActivity(intent);
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<RoomIDResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.like)
    void onLike(){
        if (mSharedPrefManager.getLoginStatus()){
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).like(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),auctionDetailsModel.getId()).enqueue(new Callback<LikeResponse>() {
                @Override
                public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getKey().equals("1")){
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                            if (response.body().getIsLike().equals("1")){
                                like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_eye));
                            }else if (response.body().getIsLike().equals("0")){
                                like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.nouneye));
                            }
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<LikeResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.share1)
    void onShare(){
        CommonUtil.ShareProductName(mContext,auctionDetailsModel.getLink_share());
    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        mContext.startActivity(intent);
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions((Activity)mContext,PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    @OnClick(R.id.call)
    void onCall(){
        getLocationWithPermission(auctionDetailsModel.getAdvertiser_phone());
    }
}


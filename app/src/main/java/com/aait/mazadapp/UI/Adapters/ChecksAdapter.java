package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.CheckModel;
import com.aait.mazadapp.R;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;

public class ChecksAdapter extends ParentRecyclerAdapter<CheckModel> {
    public ChecksAdapter(Context context, List<CheckModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ChecksAdapter.ViewHolder holder = new ChecksAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ChecksAdapter.ViewHolder viewHolder = (ChecksAdapter.ViewHolder) holder;
        final CheckModel checkmodel = data.get(position);
        Log.e("check" ,new Gson().toJson(checkmodel));
        viewHolder.branch.setText(checkmodel.getBranch());
        viewHolder.start_date.setText(checkmodel.getDay_from());
        viewHolder.end_date.setText(checkmodel.getDay_to()+"");
        viewHolder.start_time.setText(checkmodel.getTime_from()+"");
        viewHolder.end_time.setText(checkmodel.getTime_to()+"");
    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.branch)
        TextView branch;
        @BindView(R.id.start_date)
        TextView start_date;
        @BindView(R.id.end_date)
        TextView end_date;
        @BindView(R.id.start_time)
        TextView start_time;
        @BindView(R.id.end_time)
        TextView end_time;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

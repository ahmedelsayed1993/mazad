package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.AucationsResponse;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.AuctionsAdapter;
import com.aait.mazadapp.UI.Fragments.ActiveFragment;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchByCategoryActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    CategoriesModel categoriesModel;
    @BindView(R.id.search_text)
    EditText search_text;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<AucationsModel> aucationsModels = new ArrayList<>();
    AuctionsAdapter auctionsAdapter;
    @Override
    protected void initializeComponents() {
        categoriesModel = (CategoriesModel) getIntent().getSerializableExtra("category");
        title.setText(categoriesModel.getName());
        search.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        auctionsAdapter = new AuctionsAdapter(mContext,aucationsModels,R.layout.recycler_aucations);
        auctionsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(auctionsAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                search_text.setText("");
            }
        });

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_search;
    }
    @OnClick(R.id.search_icon)
    void onSearch(){
        if (search_text.getText().toString().trim().isEmpty()){

        }else {
            search(search_text.getText().toString());
        }
    }

    private void search(String text){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).searchByCat(mLanguagePrefManager.getAppLanguage(),text,categoriesModel.getId()).enqueue(new Callback<AucationsResponse>() {
            @Override
            public void onResponse(Call<AucationsResponse> call, Response<AucationsResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            aucationsModels = response.body().getData();
                            linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
                            auctionsAdapter = new AuctionsAdapter(mContext,response.body().getData(),R.layout.recycler_aucations);
                            auctionsAdapter.setOnItemClickListener(SearchByCategoryActivity.this);
                            rvRecycle.setLayoutManager(linearLayoutManager);
                            rvRecycle.setAdapter(auctionsAdapter);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AucationsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.image){
            Intent intent = new Intent(mContext, ImageActivity.class);
            intent.putExtra("path",aucationsModels.get(position).getImage());
            startActivity(intent);
        }else {
            Intent intent = new Intent(mContext, AuctionDetailsActivity.class);
            intent.putExtra("category", aucationsModels.get(position).getCategory());
            Log.e("id", aucationsModels.get(position).getId() + "");
            intent.putExtra("auction", aucationsModels.get(position).getId() + "");
            startActivity(intent);
        }
    }
}

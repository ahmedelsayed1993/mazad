package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.media.AsyncPlayer;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.SubscribeTapAdapter;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;


import butterknife.BindView;
import butterknife.OnClick;

public class AucationsActivity extends ParentActivity {
    @OnClick(R.id.back)
    void onBack(){
        startActivity(new Intent(mContext,MainActivity.class));
        AucationsActivity.this.finish();
    }
    @BindView(R.id.orders)
    TabLayout myOrdersTab;
    @BindView(R.id.ordersViewPager)
    ViewPager myOrdersViewPager;
    CategoriesModel categoriesModel;

    private SubscribeTapAdapter mAdapter;

    @BindView(R.id.title)
    TextView title;
    String type;
    @Override
    protected void initializeComponents() {
        categoriesModel = (CategoriesModel) getIntent().getSerializableExtra("category");
        Log.e("cat",new Gson().toJson(categoriesModel));
        type = getIntent().getStringExtra("type");
        title.setText(categoriesModel.getName());

        if (type.equals("normal")){
            mAdapter = new SubscribeTapAdapter(mContext,getSupportFragmentManager(),categoriesModel);
            myOrdersViewPager.setAdapter(mAdapter);
            myOrdersTab.setupWithViewPager(myOrdersViewPager);


        }else {
            mAdapter = new SubscribeTapAdapter(mContext,getSupportFragmentManager(),categoriesModel);
            myOrdersViewPager.setAdapter(mAdapter);
//            myOrdersViewPager.setCurrentItem(2,true);
            myOrdersTab.setupWithViewPager(myOrdersViewPager);
            myOrdersTab.getTabAt(1).select();

        }

//        Calendar calendar = new GregorianCalendar();
//        TimeZone timeZone = TimeZone.getTimeZone("Asia/Riyadh");
//        calendar.setTimeZone(timeZone);
//        long timeCPH = calendar.getTimeInMillis();
//        Log.e("current",timeCPH+"");
//
//        final long milliseconds = 1579861200000L-timeCPH;
//        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);
//        Log.e("day",day+"");
//        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);
//
//        Log.e("hour",hours+"");
//        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
//                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
//        Log.e("minute",minutes+"");
//        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
//                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));
//        Log.e("seconds",seconds+"");
//        final long ms = TimeUnit.MILLISECONDS.toMillis(milliseconds)
//                - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(milliseconds));
////        text.setText(String.format("%d Days %d Hours %d Minutes %d Seconds %d Milliseconds", day, hours, minutes, seconds, ms));
//
//        new CountDownTimer(milliseconds, 1000) { // adjust the milli seconds here
//
//            public void onTick(long millisUntilFinished) {
//
//                text.setText(String.format("%d Day %d Hour %d Minute %d Second",TimeUnit.MILLISECONDS.toDays(millisUntilFinished),
//                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)-TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
//                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
//                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
//                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
//                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
//            }
//
//            public void onFinish() {
//                text.setText("done!");
//            }
//        }.start();
//
//
    }
    @OnClick(R.id.search)
    void onSearch(){
        Intent intent = new Intent(mContext,SearchByCategoryActivity.class);
        intent.putExtra("category",categoriesModel);
        startActivity(intent);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_aucations;
    }
}

package com.aait.mazadapp.UI.Fragments;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.AboutActivity;
import com.aait.mazadapp.UI.Activities.ContactUsActivity;
import com.aait.mazadapp.UI.Activities.LanguageActivity;
import com.aait.mazadapp.UI.Activities.LoginActivity;
import com.aait.mazadapp.UI.Activities.MyLikesActivity;
import com.aait.mazadapp.UI.Activities.MySubScriptions;
import com.aait.mazadapp.UI.Activities.ProfileActivity;
import com.aait.mazadapp.UI.Activities.QuestionActivity;
import com.aait.mazadapp.UI.Activities.SplashActivity;
import com.aait.mazadapp.UI.Activities.SugestionActivity;
import com.aait.mazadapp.UI.Activities.TermsActivity;
import com.aait.mazadapp.UI.Activities.WinnerActivity;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {
    @BindView(R.id.profile)
    CircleImageView profile;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    String newToken = null;
    public static ProfileFragment newInstance() {
        Bundle args = new Bundle();
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void initializeComponents(View view) {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( getActivity(),  new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken",newToken);

            }
        });
        if (mSharedPrefManager.getLoginStatus()) {
            getProfile();
        }else {

        }
    }
    @OnClick(R.id.logout)
    void onLogout(){
        if (mSharedPrefManager.getLoginStatus()) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).logout(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getId(), newToken, "android").enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                        mSharedPrefManager.setLoginStatus(false);
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext, SplashActivity.class));

                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUser(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Glide.with(mContext).load(response.body().getData().getAvatar()).into(profile);
                        name.setText(response.body().getData().getName());
                        email.setText(response.body().getData().getEmail());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.about)
    void onAbout(){
        ActivityOptions options =
                ActivityOptions.makeCustomAnimation(mContext, R.anim.layout_animation_slide_right, R.anim.layout_animation_fall_down);
        startActivity(new Intent(mContext, AboutActivity.class),options.toBundle());
    }
    @OnClick(R.id.terms)
    void onTerms(){
        startActivity(new Intent(mContext, TermsActivity.class));
    }
    @OnClick(R.id.phone)
    void onPhone(){
        startActivity(new Intent(mContext, ContactUsActivity.class));
    }
    @OnClick(R.id.follow)
    void onFollow(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, MyLikesActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.auction)
    void onAuction(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, MySubScriptions.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.lang)
    void onLang(){
        startActivity(new Intent(mContext, LanguageActivity.class));
    }
    @OnClick(R.id.question)
    void onQuestion(){
        startActivity(new Intent(mContext, QuestionActivity.class));
    }
    @OnClick(R.id.suggestion)
    void onSuggestion(){
        startActivity(new Intent(mContext, SugestionActivity.class));
    }
    @OnClick(R.id.win)
    void onWin(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, WinnerActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.data)
    void onProfile(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, ProfileActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
}

package com.aait.mazadapp.UI.Activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Gps.GPSTracker;
import com.aait.mazadapp.Gps.GpsTrakerListener;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.AuctionDetailsModel;
import com.aait.mazadapp.Models.AuctionDetailsResponse;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.ImageModel;
import com.aait.mazadapp.Models.LikeResponse;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.Models.RoomIDResponse;
import com.aait.mazadapp.Models.SubScripResponse;
import com.aait.mazadapp.Models.TendersModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.DetailsAdapter;
import com.aait.mazadapp.UI.Adapters.RecyclerPoupupAds;
import com.aait.mazadapp.UI.Adapters.TenderAdapter;
import com.aait.mazadapp.UI.Views.SubscribeDialog;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuctionDetailsActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,GoogleMap.OnMarkerClickListener, GpsTrakerListener{
    GoogleMap googleMap;
    Geocoder geocoder;
    GPSTracker gps;
    @BindView(R.id.lay_no_internet)
    RelativeLayout lay_no_internet;

    public String mLang, mLat;
    boolean startTracker = false;
    private AlertDialog mAlertDialog;
    @BindView(R.id.three_d)
    CardView three_d;
    Marker myMarker;
    String  aucationsModel;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.current_price)
    TextView current_price;
    @BindView(R.id.lowest_price)
    TextView lowest_price;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.day)
    TextView days;
    @BindView(R.id.hour)
    TextView hour;
    @BindView(R.id.miute)
    TextView miute;
    @BindView(R.id.second)
    TextView second;
    @BindView(R.id.end)
    LinearLayout end;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.map)
    MapView map;
    @BindView(R.id.viewpager1)
    ViewPager viewPager;
    ArrayList<ListModel> listModels = new ArrayList<>();
    ArrayList<ImageModel> imageModels = new ArrayList<>();
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RecyclerPoupupAds recyclerPoupupAds;
    @BindView(R.id.profile)
    CircleImageView profile;
    @BindView(R.id.tender)
    TextView tender;
    @BindView(R.id.tender_price)
    TextView tender_price;
    @BindView(R.id.tender_lay)
    LinearLayout tender_lay;
    @OnClick(R.id.back)
    void onBack(){
       onBackPressed();
    }
    @BindView(R.id.all_tender)
    RecyclerView all_tenders;
    TenderAdapter tenderAdapter;
    ArrayList<TendersModel> tendersModels = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    AuctionDetailsModel auctionDetailsModel;
    String categoriesModel;
    @BindView(R.id.number)
    TextView number;
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.advertiser_name)
    TextView advertiser_name;
    @BindView(R.id.advertiser_phone)
    TextView advertiser_phone;
    @BindView(R.id.details)
    RecyclerView details;
    LinearLayoutManager linearLayoutManager1;
    DetailsAdapter detailsAdapter;
    @BindView(R.id.dots_indicator)
    SpringDotsIndicator indicator;
    @BindView(R.id.higry)
    TextView higry;
    @BindView(R.id.like)
    ImageView like;
    @BindView(R.id.terms)
    ToggleButton terms;

    @Override
    protected void initializeComponents() {
        aucationsModel =  getIntent().getStringExtra("auction");
        categoriesModel =  getIntent().getStringExtra("category");

        map.onCreate(mSavedInstanceState);
        map.onResume();
        map.getMapAsync(this);

        try {
            MapsInitializer.initialize(getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        advertiser_phone.setVisibility(View.GONE);
        getLocationWithPermission();
        if (categoriesModel.equals("plates")){
            three_d.setVisibility(View.GONE);
        }else {
            three_d.setVisibility(View.VISIBLE);
        }

        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        tenderAdapter = new TenderAdapter(mContext,tendersModels,R.layout.recycler_tender);
        all_tenders.setLayoutManager(linearLayoutManager);
        all_tenders.setAdapter(tenderAdapter);
        if(mSharedPrefManager.getLoginStatus()) {
            getAuction(mSharedPrefManager.getUserData().getId() + "");
        }else {
            getAuction(null);
        }

    }
    void setData(AuctionDetailsModel auctionDetailsModel){
        imageModels = auctionDetailsModel.getImages();
        recyclerPoupupAds = new RecyclerPoupupAds(mContext,imageModels);
        viewPager.setAdapter(recyclerPoupupAds);

        indicator.setViewPager(viewPager);


        NUM_PAGES = recyclerPoupupAds.getCount();
        final Handler handler = new Handler();
        final Runnable update = new Runnable() {
            @Override
            public void run() {
                if (currentPage == NUM_PAGES){ currentPage = 0; }
                viewPager.setCurrentItem(currentPage,true);
                currentPage++;

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);


            }
        }, 2500,2500);
        higry.setText(auctionDetailsModel.getEnd_date_hijri());
        if (auctionDetailsModel.getImage_3D().trim().equals("")){
            three_d.setVisibility(View.GONE);
        }else {
            three_d.setVisibility(View.VISIBLE);
        }
        listModels.clear();
        listModels.add(new ListModel(getString(R.string.Announcement_No),auctionDetailsModel.getId()+""));
         if (auctionDetailsModel.getBrand_id().equals("")){

         }else {
             listModels.add(new ListModel(getString(R.string.Car_brand),auctionDetailsModel.getBrand_id()));
         }
         if (auctionDetailsModel.getCount_distances().equals("")){

         }else {
             listModels.add(new ListModel(getString(R.string.odometer),auctionDetailsModel.getCount_distances()));
         }
        if (auctionDetailsModel.getModel_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.car_model),auctionDetailsModel.getModel_id()));
        }
        if (auctionDetailsModel.getYear_make().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.manufacturing_year),auctionDetailsModel.getYear_make()));
        }
        if (auctionDetailsModel.getVehicle_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Engine_type),auctionDetailsModel.getVehicle_id()));
        }
        if (auctionDetailsModel.getCity_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.City),auctionDetailsModel.getCity_id()));
        }
        if (auctionDetailsModel.getRegion_id().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Region),auctionDetailsModel.getRegion_id()));
        }
        if (auctionDetailsModel.getCount_plate_numbers().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.numbers_count),auctionDetailsModel.getCount_plate_numbers()));
        }
        if (auctionDetailsModel.getPlate_number().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.plate_number),auctionDetailsModel.getPlate_number()));
        }
        if (auctionDetailsModel.getProperty_type().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.Property_type),auctionDetailsModel.getProperty_type()));
        }
        if (auctionDetailsModel.getProperty_area().equals("")){

        }else {
            listModels.add(new ListModel(getString(R.string.The_space),auctionDetailsModel.getProperty_area()));
        }
        if (auctionDetailsModel.getIsLike()==1){
            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_eye));
        }else {
            like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.nouneye));
        }
        Log.e("details",new Gson().toJson(listModels));
        name.setText(auctionDetailsModel.getTitle());
        date.setText(auctionDetailsModel.getStart_date());
        current_price.setText(auctionDetailsModel.getLast_price()+" "+getString(R.string.RS));
        lowest_price.setText(auctionDetailsModel.getStarting_price()+" "+getString(R.string.RS));
        price.setText(auctionDetailsModel.getLast_price()+"");
        description.setText(auctionDetailsModel.getDescription());
        Glide.with(mContext).load(auctionDetailsModel.getAdvertiser_avatar()).into(image);
        advertiser_name.setText(auctionDetailsModel.getAdvertiser_name());
        advertiser_phone.setText(auctionDetailsModel.getAdvertiser_phone());
        linearLayoutManager1 = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        detailsAdapter = new DetailsAdapter(mContext,listModels,R.layout.recycler_details);
        details.setLayoutManager(linearLayoutManager1);
        details.setAdapter(detailsAdapter);
        putMapMarker(Double.parseDouble(auctionDetailsModel.getLat()),Double.parseDouble(auctionDetailsModel.getLng()));
        if (auctionDetailsModel.getTenders().size()==0){
            tender_lay.setVisibility(View.GONE);
        }else {
            tender_lay.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(auctionDetailsModel.getTenders().get(0).getAvatar()).into(profile);
            tender.setText(auctionDetailsModel.getTenders().get(0).getName());
            tender_price.setText(auctionDetailsModel.getTenders().get(0).getPrice()+getString(R.string.RS));
        }
        if (auctionDetailsModel.getTenders().size()>4){
            tenderAdapter.Insert(0,auctionDetailsModel.getTenders().get(0));
            tenderAdapter.Insert(1,auctionDetailsModel.getTenders().get(1));
            tenderAdapter.Insert(2,auctionDetailsModel.getTenders().get(2));
            tenderAdapter.Insert(3,auctionDetailsModel.getTenders().get(3));
            number.setVisibility(View.VISIBLE);
            number.setText("+"+(auctionDetailsModel.getTenders().size()-4));
        }else {
            tenderAdapter.updateAll(auctionDetailsModel.getTenders());
            number.setVisibility(View.GONE);
        }
        putMapMarker(Double.parseDouble(auctionDetailsModel.getLat()),Double.parseDouble(auctionDetailsModel.getLng()));
        Calendar calendar = new GregorianCalendar();
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Riyadh");
        calendar.setTimeZone(timeZone);
        long timeCPH = calendar.getTimeInMillis();
        Log.e("current",timeCPH+"");

        final long milliseconds = auctionDetailsModel.getEnd_date()-timeCPH;
        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);
        Log.e("day",day+"");
        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);

        Log.e("hour",hours+"");
        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
        Log.e("minute",minutes+"");
        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));
        Log.e("seconds",seconds+"");
        final long ms = TimeUnit.MILLISECONDS.toMillis(milliseconds)
                - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(milliseconds));
//        text.setText(String.format("%d Days %d Hours %d Minutes %d Seconds %d Milliseconds", day, hours, minutes, seconds, ms));

        new CountDownTimer(milliseconds, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {

                days.setText(String.format(("%d "+getString(R.string.Day) ),TimeUnit.MILLISECONDS.toDays(millisUntilFinished)));
                hour.setText(String.format(("%d "+getString(R.string.Hour) ),TimeUnit.MILLISECONDS.toHours(millisUntilFinished)-TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished))));
                miute.setText(String.format(("%d "+getString(R.string.Minute)),TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished))));
                second.setText(String.format(("%d "+getString(R.string.Second)),TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

            }

            public void onFinish() {
                end.setVisibility(View.GONE);
            }
        }.start();
        if (auctionDetailsModel.getShow_counter()==1){

          end.setVisibility(View.VISIBLE);
        }else if (auctionDetailsModel.getShow_counter()==0){
            end.setVisibility(View.GONE);
        }
    }
    @OnClick(R.id.price)
    void onPrice(){
        price.setCursorVisible(true);
        price.setFocusableInTouchMode(true);
        price.requestFocus();
        price.setEnabled(true);
    }
    @OnClick(R.id.plus)
    void onPlus(){
        int price1 = 0;
        price1=Integer.parseInt(price.getText().toString())+50;
        price.setText(price1+"");

    }
    @OnClick(R.id.minus)
    void onMinus(){
        if (Integer.parseInt(price.getText().toString())>Integer.parseInt(auctionDetailsModel.getLast_price())){
            int price1 = 0;
            price1=Integer.parseInt(price.getText().toString())-50;
            price.setText(price1+"");
        }else {

        }
    }
    @OnClick(R.id.like)
    void onLike(){
        if (mSharedPrefManager.getLoginStatus()){
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).like(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),auctionDetailsModel.getId()).enqueue(new Callback<LikeResponse>() {
                @Override
                public void onResponse(Call<LikeResponse> call, Response<LikeResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getKey().equals("1")){
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                            if (response.body().getIsLike().equals("1")){
                                like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.noun_eye));
                            }else if (response.body().getIsLike().equals("0")){
                                like.setImageDrawable(mContext.getResources().getDrawable(R.mipmap.nouneye));
                            }
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<LikeResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.Terms)
    void onTerms(){
        startActivity(new Intent(mContext,TermsActivity.class));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_auction_details;
    }
    private void getAuction(String id){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getAuction(mLanguagePrefManager.getAppLanguage(), Integer.parseInt(aucationsModel),id).enqueue(new Callback<AuctionDetailsResponse>() {
            @Override
            public void onResponse(Call<AuctionDetailsResponse> call, Response<AuctionDetailsResponse> response) {
                hideProgressDialog();
                lay_no_internet.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        auctionDetailsModel = response.body().getData();
                        Log.e("response",new Gson().toJson(response.body().getData()));
                        setData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuctionDetailsResponse> call, Throwable t) {
                 CommonUtil.handleException(mContext,t);
                 lay_no_internet.setVisibility(View.VISIBLE);
                 t.printStackTrace();
                 hideProgressDialog();
            }
        });
    }
    @OnClick(R.id.three_d)
    void onThree(){
        Log.e("image",auctionDetailsModel.getImage_3D());
        if (auctionDetailsModel.getImage_3D().trim().isEmpty()){

        }else {
            Intent intent = new Intent(mContext, ThreeDimActivity.class);
            intent.putExtra("three", auctionDetailsModel.getImage_3D());
            intent.putExtra("lang", mLanguagePrefManager.getAppLanguage());
            startActivity(intent);
        }
    }
    private void Tender(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).tender(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),auctionDetailsModel.getId(),price.getText().toString()).enqueue(new Callback<SubScripResponse>() {
            @Override
            public void onResponse(Call<SubScripResponse> call, Response<SubScripResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Intent intent = new Intent(mContext,BackToHomeActivity.class);
                        intent.putExtra("category",categoriesModel);

                        intent.putExtra("auction",aucationsModel);
                        startActivity(intent);
                        AuctionDetailsActivity.this.finish();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SubScripResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.participate)
    void onparticipate(){
        if (mSharedPrefManager.getLoginStatus()) {
            if (terms.isChecked()){
            if (price.getText().toString().trim().equals("")){

            }else {
            if (auctionDetailsModel.getSubscribed() == 0) {

                if (auctionDetailsModel.getAmount_insurance().equals("")) {
                    if (Integer.parseInt(price.getText().toString()) <= (Integer.parseInt(auctionDetailsModel.getLast_price())+49)) {
                        CommonUtil.makeToast(mContext, getString(R.string.can_not));
                    } else {


                            Tender();

                    }
                } else {
                    new SubscribeDialog(mContext, auctionDetailsModel).show();
                }

            } else {
                if (Integer.parseInt(price.getText().toString()) <= (Integer.parseInt(auctionDetailsModel.getLast_price())+49)) {
                    CommonUtil.makeToast(mContext, getString(R.string.can_not));
                } else {


                        Tender();

                }

            }
            }}
            else {
                CommonUtil.makeToast(mContext,getString(R.string.terms_conditions));
            }
        }else {
            new VisitorDialog(mContext).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getAuction();
        getLocationWithPermission();
    }

    @OnClick(R.id.message)
    void onMessage(){
        if (mSharedPrefManager.getLoginStatus()) {
            showProgressDialog(getString(R.string.please_wait));
            RetroWeb.getClient().create(ServiceApi.class).getRoom(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getId(), auctionDetailsModel.getId()).enqueue(new Callback<RoomIDResponse>() {
                @Override
                public void onResponse(Call<RoomIDResponse> call, Response<RoomIDResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {

                            Intent intent = new Intent(mContext, ChatActivity.class);
                            intent.putExtra("room", response.body().getData().getConversation_id() + "");
                            intent.putExtra("auction", auctionDetailsModel.getId() + "");
                            intent.putExtra("user", auctionDetailsModel.getAdvertiser_name());
                            intent.putExtra("lastpage", response.body().getData().getLastPage() + "");
                            intent.putExtra("reciver", auctionDetailsModel.getAdvertiser_id() + "");
                            startActivity(intent);
                        } else {
                            CommonUtil.makeToast(mContext, response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<RoomIDResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext, t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.share1)
    void onShare(){
        CommonUtil.ShareProductName(mContext,auctionDetailsModel.getLink_share());
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {


                //LatLngBound will cover all your marker on Google Maps





            }
        });
    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(mContext, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(getApplicationContext(), PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
//                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
//            getCurrentLocation();
        }

    }
    public void putMapMarker(Double lat, Double log) {
        if (googleMap!=null) {
            LatLng latLng = new LatLng(lat, log);

            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(10));
            myMarker = googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("موقعى")
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.location_red)));
        }

    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                //putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        mContext.startActivity(intent);
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
                    ActivityCompat.requestPermissions((Activity)mContext,PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }
    @OnClick(R.id.call)
    void onCall(){
        getLocationWithPermission(auctionDetailsModel.getAdvertiser_phone());
    }
}

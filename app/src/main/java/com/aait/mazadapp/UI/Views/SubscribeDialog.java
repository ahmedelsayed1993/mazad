package com.aait.mazadapp.UI.Views;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Models.AuctionDetailsModel;
import com.aait.mazadapp.Models.SubScripResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.Pereferences.LanguagePrefManager;
import com.aait.mazadapp.Pereferences.SharedPrefManager;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.AuctionDetailsActivity;
import com.aait.mazadapp.UI.Activities.WalletActivity;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.DialogUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscribeDialog extends Dialog {
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    AuctionDetailsModel auctionDetailsModel;
    @BindView(R.id.price)
    TextView price;
    private ProgressDialog mProgressDialog;
    public SubscribeDialog(@NonNull Context context,AuctionDetailsModel auctionDetailsModel) {
        super(context);
        this.mContext = context;
        this.auctionDetailsModel = auctionDetailsModel;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_subscribe);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);

        setCancelable(false);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        price.setText(auctionDetailsModel.getAmount_insurance()+"  "+mContext.getResources().getString(R.string.RS));

    }
    @OnClick(R.id.discount)
    void onDiscount(){
        mProgressDialog = DialogUtil.showProgressDialog(mContext, mContext.getString(R.string.please_wait), false);
        RetroWeb.getClient().create(ServiceApi.class).subscrip(languagePrefManager.getAppLanguage(),sharedPreferences.getUserData().getId(),auctionDetailsModel.getId()).enqueue(new Callback<SubScripResponse>() {
            @Override
            public void onResponse(Call<SubScripResponse> call, Response<SubScripResponse> response) {
                mProgressDialog.dismiss();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        SubscribeDialog.this.dismiss();
                        Intent intent = new Intent(mContext, AuctionDetailsActivity.class);
                        intent.putExtra("auction",auctionDetailsModel.getId()+"");
                        intent.putExtra("category",auctionDetailsModel.getCategory_key());
                        mContext.startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SubScripResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                mProgressDialog.dismiss();

            }
        });
    }
    @OnClick(R.id.increase)
    void onIncrease(){
        mContext.startActivity(new Intent(mContext, WalletActivity.class));
        SubscribeDialog.this.dismiss();
    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.aait.mazadapp.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazadapp.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.lay_no_internet)
    RelativeLayout lay_no_internet;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.profile)
    CircleImageView profile;
    @BindView(R.id.user_name)
    TextView user_name;
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.name)
    TextView name;
    private String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.My_account_information));
        search.setVisibility(View.GONE);
        getProfile();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_my_data;
    }
    @OnClick(R.id.my_data)
    void onData(){
        startActivity(new Intent(mContext,EditDataActivity.class));
    }
    @OnClick(R.id.password)
    void onPassword(){
        startActivity(new Intent(mContext,EditPasswordActivity.class));
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUser(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                lay_no_internet.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        Glide.with(mContext).load(response.body().getData().getAvatar()).into(profile);
                        name.setText(response.body().getData().getName());
                        address.setText(response.body().getData().getAddress());
                        phone.setText(response.body().getData().getPhone());
                        user_name.setText(response.body().getData().getName());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                lay_no_internet.setVisibility(View.VISIBLE);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }

    @OnClick(R.id.profile)
    void onAvatar(){
        getPickImageWithPermission();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
                Log.e("image",new Gson().toJson(returnValue));

                ImageBasePath = returnValue.get(0);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                profile.setImageURI(Uri.parse(ImageBasePath));
                if (ImageBasePath!=null) {
                    updateImage(ImageBasePath);
                }
            }
        }
    }

    private void updateImage(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateImage(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),filePart).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getKey().equals("1")){

                        Glide.with(mContext).load(response.body().getData().getAvatar()).into(profile);
                        mSharedPrefManager.setUserData(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}

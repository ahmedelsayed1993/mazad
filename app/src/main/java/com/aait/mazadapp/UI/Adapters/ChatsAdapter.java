package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.ChatsModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatsAdapter extends ParentRecyclerAdapter<ChatsModel> {
    public ChatsAdapter(Context context, List<ChatsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ChatsAdapter.ViewHolder holder = new ChatsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ChatsAdapter.ViewHolder viewHolder = (ChatsAdapter.ViewHolder) holder;
        final ChatsModel chatsModel = data.get(position);
        Glide.with(mcontext).load(chatsModel.getAvatar()).into(viewHolder.image);
        viewHolder.name.setText(chatsModel.getUsername());
        viewHolder.time.setText(chatsModel.getDate());
        viewHolder.message.setText(chatsModel.getMessage());
        if (chatsModel.getSeen()==1){
            viewHolder.read.setVisibility(View.GONE);

        }else {
            viewHolder.read.setVisibility(View.VISIBLE);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.message)
        TextView message;
        @BindView(R.id.read)
        TextView read;







        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

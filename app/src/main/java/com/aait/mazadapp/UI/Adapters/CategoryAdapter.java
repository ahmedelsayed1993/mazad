package com.aait.mazadapp.UI.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryAdapter extends ParentRecyclerAdapter<CategoriesModel> {
    public CategoryAdapter(Context context, List<CategoriesModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        CategoryAdapter.ViewHolder holder = new CategoryAdapter.ViewHolder(itemView);
        return holder;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final CategoryAdapter.ViewHolder viewHolder = (CategoryAdapter.ViewHolder) holder;
        final CategoriesModel categoriesModel = data.get(position);

//        viewHolder.back.setBackgroundColor(Color.parseColor(categoriesModel.getBackground_color()));
        Glide.with(mcontext).load(categoriesModel.getImage()).into(viewHolder.image);
       viewHolder.count.setTextColor(Color.parseColor(categoriesModel.getColor()));
        viewHolder.name.setTextColor(Color.parseColor(categoriesModel.getColor()));
        viewHolder.name.setText(categoriesModel.getName());
        viewHolder.count.setText("("+categoriesModel.getAuctions_count()+")");
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.count)
        TextView count;
//        @BindView(R.id.back)
//        ImageView back;
        @BindView(R.id.image)
        ImageView image;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Models.SocialModel;
import com.aait.mazadapp.Models.SugestionResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.SocialAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Query;

public class SugestionActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.whats)
    TextView whats;
    @BindView(R.id.social)
    RecyclerView social;
    ArrayList<SocialModel> socialModels = new ArrayList<>();
    SocialAdapter socialAdapter;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.topic)
    EditText topic;
    @BindView(R.id.complain)
    EditText complain;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.Complaints_and_suggestions));
        search.setVisibility(View.GONE);
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        socialAdapter = new SocialAdapter(mContext,socialModels,R.layout.recycler_social);
        socialAdapter.setOnItemClickListener(this);
        social.setLayoutManager(linearLayoutManager);
        social.setAdapter(socialAdapter);
        getComplain(null,null,null);
        if (mSharedPrefManager.getLoginStatus()){
            name.setText(mSharedPrefManager.getUserData().getName());
        }else {
            name.setText("");
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_complains;
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,topic,getString(R.string.Topic))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,complain,getString(R.string.Write_your_complaint_or_suggestion))){
            return;
        }else {
            writeComplain(name.getText().toString(),topic.getText().toString(),complain.getText().toString());
        }
    }
    private void getComplain(String name,String topic,String complain){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).suggestion(mLanguagePrefManager.getAppLanguage(),name,topic,complain).enqueue(new Callback<SugestionResponse>() {
            @Override
            public void onResponse(Call<SugestionResponse> call, Response<SugestionResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                            phone.setText(response.body().getPhones().getPhone());
                            whats.setText(response.body().getPhones().getWhatsapp());
                            socialAdapter.updateAll(response.body().getData());



                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SugestionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }
    private void writeComplain(String name,String topic,String complain){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).writesuggestion(mLanguagePrefManager.getAppLanguage(),name,topic,complain).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    @Override
    public void onItemClick(View view, int position) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(socialModels.get(position).getLink()));
        startActivity(i);
    }
}

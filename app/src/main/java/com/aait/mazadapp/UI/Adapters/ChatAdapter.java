package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.mazadapp.Models.MessageModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    Context context;
    List<MessageModel> messageModels;
    int id;
    LinearLayout.LayoutParams paramsMsg;
    public ChatAdapter(Context context, List<MessageModel> messageModels, int id) {
        this.context = context;
        this.messageModels = messageModels;
        this.id = id;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v;
        if (viewType == 0) {
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat_send, parent, false));
        } else
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_chat_receive, parent, false));
    }

    @Override
    public int getItemViewType(int position) {

        if (messageModels.get(position).getUser_id()==id) {

            return 0;
        } else {

            return 1;
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.msg.setText(messageModels.get(position).getMessage());
        holder.date.setText(messageModels.get(position).getCreated());
        Glide.with(context).load(messageModels.get(position).getAvatar()).into(holder.image);


    }

    @Override
    public int getItemCount() {
        return messageModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView msg;
        TextView date;
        CircleImageView image;



        public ViewHolder(View itemView) {
            super(itemView);
            msg = itemView.findViewById(R.id.tv_name);
            date = itemView.findViewById(R.id.tv_date);
            image = itemView.findViewById(R.id.sender);


        }
    }
}

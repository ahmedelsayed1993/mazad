package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.WinnerActivity;
import com.bumptech.glide.Glide;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class WinnerAdapter extends ParentRecyclerAdapter<AucationsModel> {
    public WinnerAdapter(Context context, List<AucationsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        WinnerAdapter.ViewHolder holder = new WinnerAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final WinnerAdapter.ViewHolder viewHolder = (WinnerAdapter.ViewHolder) holder;
        final AucationsModel aucationsModel = data.get(position);
        viewHolder.times.setVisibility(View.GONE);
        viewHolder.auction.setText(aucationsModel.getOwner_name());
        viewHolder.bidding.setText(aucationsModel.getTendersCount()+mcontext.getResources().getString(R.string.Bidding));
        viewHolder.last_price.setText(aucationsModel.getLast_price()+mcontext.getResources().getString(R.string.RS));
        viewHolder.name.setText(aucationsModel.getTitle());
        Glide.with(mcontext).load(aucationsModel.getOwner_image()).into(viewHolder.profile);
        Glide.with(mcontext).load(aucationsModel.getImage()).into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.last_price)
        TextView last_price;
        @BindView(R.id.profile)
        CircleImageView profile;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.times)
        LinearLayout times;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.bidding)
        TextView bidding;
        @BindView(R.id.auction)
        TextView auction;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }

}

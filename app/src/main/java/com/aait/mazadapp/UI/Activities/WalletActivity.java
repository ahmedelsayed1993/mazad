package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.SubScripResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends ParentActivity {
    @BindView(R.id.lay_no_internet)
    RelativeLayout lay_no_internet;
    @BindView(R.id.balance)
    TextView balance;
    @OnClick(R.id.back)
    void onBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id.title)
    TextView title;
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.wallet));
         getBanlance();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_wallet;
    }
    private void getBanlance(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).financial(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<SubScripResponse>() {
            @Override
            public void onResponse(Call<SubScripResponse> call, Response<SubScripResponse> response) {
                hideProgressDialog();
                lay_no_internet.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        balance.setText(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<SubScripResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                lay_no_internet.setVisibility(View.VISIBLE);
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.credit)
    void onCredit(){
        startActivity(new Intent(mContext,AccountsActivity.class));
    }
}

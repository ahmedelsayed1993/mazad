package com.aait.mazadapp.UI.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.CategoriesResponse;
import com.aait.mazadapp.Models.ImageModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.AucationsActivity;
import com.aait.mazadapp.UI.Activities.NotificationActivity;
import com.aait.mazadapp.UI.Activities.SearchActivity;
import com.aait.mazadapp.UI.Activities.WalletActivity;
import com.aait.mazadapp.UI.Adapters.CategoryAdapter;
import com.aait.mazadapp.UI.Adapters.SliderAdapterExample;
import com.aait.mazadapp.UI.Views.VisitorDialog;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends BaseFragment implements OnItemClickListener {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    GridLayoutManager gridLayoutManager;
    ArrayList<CategoriesModel> categoriesModels = new ArrayList<>();
    CategoryAdapter categoryAdapter;
    @BindView(R.id.imageSlider)
    SliderView sliderView;
    List<ImageModel> imageModels = new ArrayList<>();
    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initializeComponents(View view) {
        gridLayoutManager = new GridLayoutManager(mContext,2);
        categoryAdapter = new CategoryAdapter(mContext,categoriesModels,R.layout.recycler_categories);
        categoryAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(gridLayoutManager);
        rvRecycle.setAdapter(categoryAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCategories();
            }
        });
        getCategories();






    }
    @OnClick(R.id.notification)
    void onNotification(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, NotificationActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    @OnClick(R.id.wallet)
    void onWallet(){
        if (mSharedPrefManager.getLoginStatus()) {
            startActivity(new Intent(mContext, WalletActivity.class));
        }else {
            new VisitorDialog(mContext).show();
        }
    }
    void setSlider(List<ImageModel> categoriesModels){
        SliderAdapterExample adapter = new SliderAdapterExample(mContext,categoriesModels);

        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(mContext.getResources().getColor(R.color.colorPrimary));
        sliderView.setIndicatorUnselectedColor(mContext.getResources().getColor(R.color.colorAccent));
        sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
        sliderView.startAutoCycle();

    }
    private void getCategories(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getCats(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            categoryAdapter.updateAll(response.body().getData());
                            setSlider(response.body().getImages());


                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @OnClick(R.id.search)
    void onSearch(){
        startActivity(new Intent(mContext, SearchActivity.class));
    }

    @Override
    public void onItemClick(View view, int position) {
        Intent intent = new Intent(mContext,AucationsActivity.class);
        intent.putExtra("category",categoriesModels.get(position));
        intent.putExtra("type","normal");
       startActivity(intent);
    }
}

package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.QuestionModel;
import com.aait.mazadapp.R;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class QuestionAdapter extends ParentRecyclerAdapter<QuestionModel> {
    public QuestionAdapter(Context context, List<QuestionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        QuestionAdapter.ViewHolder holder = new QuestionAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final QuestionAdapter.ViewHolder viewHolder = (QuestionAdapter.ViewHolder) holder;
        final QuestionModel questionModel = data.get(position);
        viewHolder.question.setText(questionModel.getQuestion());
        viewHolder.answer.setText(questionModel.getAnswer());
        viewHolder.lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
                if (viewHolder.answer.getVisibility()==View.GONE){
                    viewHolder.plus.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.min));
                    viewHolder.answer.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.plus.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.plus));
                    viewHolder.answer.setVisibility(View.GONE);
                }
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.question)
        TextView question;
        @BindView(R.id.answer)
        TextView answer;
        @BindView(R.id.lay)
        LinearLayout lay;
        @BindView(R.id.plus)
        ImageView plus;







        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.Models.TendersModel;
import com.aait.mazadapp.R;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DetailsAdapter extends ParentRecyclerAdapter<ListModel> {
    public DetailsAdapter(Context context, List<ListModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        DetailsAdapter.ViewHolder holder = new DetailsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final DetailsAdapter.ViewHolder viewHolder = (DetailsAdapter.ViewHolder) holder;
        final ListModel listModel = data.get(position);
        viewHolder.name.setText(listModel.getId());
        viewHolder.value.setText(listModel.getName());
        if (position%2==0){
            viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorOffWhite));
        }else {
            viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimaryDark));
        }
    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.value)
        TextView value;
        @BindView(R.id.lay)
        LinearLayout lay;







        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

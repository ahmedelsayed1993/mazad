package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.SocialModel;
import com.aait.mazadapp.Models.TendersModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class SocialAdapter extends ParentRecyclerAdapter<SocialModel> {
    public SocialAdapter(Context context, List<SocialModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        SocialAdapter.ViewHolder holder = new SocialAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final SocialAdapter.ViewHolder viewHolder = (SocialAdapter.ViewHolder) holder;
        final SocialModel socialModel = data.get(position);
        Glide.with(mcontext).load(socialModel.getLogo()).into(viewHolder.logo);
        viewHolder.logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {




        @BindView(R.id.logo)
        ImageView logo;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.res.Configuration;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.R;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ThreeDimActivity extends ParentActivity {
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.search)
    ImageView search;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.three_d)
    WebView three_d;
    String three;
    String lang;
    @Override
    protected void initializeComponents() {
        lang = getIntent().getStringExtra("lang");
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        three = getIntent().getStringExtra("three");
        search.setVisibility(View.GONE);
        title.setText(getString(R.string.Three_dimensional_display));
        three_d.getSettings().setJavaScriptEnabled(true);
        three_d.loadUrl(three);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_three_dem;
    }
}

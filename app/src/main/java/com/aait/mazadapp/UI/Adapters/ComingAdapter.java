package com.aait.mazadapp.UI.Adapters;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.aait.mazadapp.Base.ParentRecyclerAdapter;
import com.aait.mazadapp.Base.ParentRecyclerViewHolder;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ComingAdapter extends ParentRecyclerAdapter<AucationsModel> {
    boolean isRunning = false;
    public ComingAdapter(Context context, List<AucationsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ComingAdapter.ViewHolder holder = new ComingAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final ComingAdapter.ViewHolder viewHolder = (ComingAdapter.ViewHolder) holder;
        final AucationsModel aucationsModel = data.get(position);
        Calendar calendar = new GregorianCalendar();
        CountDownTimer countDownTimer;
        TimeZone timeZone = TimeZone.getTimeZone("Asia/Riyadh");
        calendar.setTimeZone(timeZone);
        long timeCPH = calendar.getTimeInMillis();
        Log.e("current",timeCPH+"");

        final long milliseconds = aucationsModel.getStart_date()-timeCPH;
        final long day = TimeUnit.MILLISECONDS.toDays(milliseconds);
        Log.e("day",day+"");
        final long hours = TimeUnit.MILLISECONDS.toHours(milliseconds);

        Log.e("hour",hours+"");
        final long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds)
                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milliseconds));
        Log.e("minute",minutes+"");
        final long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds)
                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(milliseconds));
        Log.e("seconds",seconds+"");
        final long ms = TimeUnit.MILLISECONDS.toMillis(milliseconds)
                - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(milliseconds));
//        text.setText(String.format("%d Days %d Hours %d Minutes %d Seconds %d Milliseconds", day, hours, minutes, seconds, ms));

       countDownTimer= new CountDownTimer(milliseconds, 1000) { // adjust the milli seconds here

            public void onTick(long millisUntilFinished) {
//                isRunning = true;

                viewHolder.time.setText(String.format(("%d "+mcontext.getResources().getString(R.string.day) +"%d "+ mcontext.getResources().getString(R.string.hour) +"%d "+mcontext.getResources().getString(R.string.miute) +"%d "+ mcontext.getResources().getString(R.string.second)),TimeUnit.MILLISECONDS.toDays(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toHours(millisUntilFinished)-TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {
//                isRunning = false;
                viewHolder.times.setVisibility(View.GONE);
                viewHolder.time.setVisibility(View.GONE);
                data.remove(position);
                try {
                    notifyDataSetChanged();
                }catch (Exception e){

                }
            }
        }.start();
//       if (isRunning){
//           countDownTimer.cancel();
//       }else {
//           countDownTimer.start();
//       }
        viewHolder.auction.setText(aucationsModel.getOwner_name());
        viewHolder.bidding.setText(aucationsModel.getTendersCount()+mcontext.getResources().getString(R.string.Bidding));
        viewHolder.last_price.setText(aucationsModel.getLast_price()+mcontext.getResources().getString(R.string.RS));
        viewHolder.name.setText(aucationsModel.getTitle());
        if (aucationsModel.getShow_counter()==1){
            viewHolder.time.setVisibility(View.VISIBLE);
            viewHolder.time_text.setText(mcontext.getString(R.string.The_rest_of_the_time));
        }else if (aucationsModel.getShow_counter()==0){
            viewHolder.time.setVisibility(View.GONE);
            viewHolder.time_text.setText(mcontext.getString(R.string.time_Appreciation));
        }
        Glide.with(mcontext).load(aucationsModel.getOwner_image()).into(viewHolder.profile);
        Glide.with(mcontext).load(aucationsModel.getImage()).apply(new RequestOptions().fitCenter().override(500,200)).into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });
        viewHolder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickListener.onItemClick(view,position);
            }
        });

    }
    public class ViewHolder extends ParentRecyclerViewHolder {



        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.last_price)
        TextView last_price;
        @BindView(R.id.profile)
        CircleImageView profile;
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.times)
        LinearLayout times;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.bidding)
        TextView bidding;
        @BindView(R.id.auction)
        TextView auction;
        @BindView(R.id.time_text)
                TextView time_text;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}

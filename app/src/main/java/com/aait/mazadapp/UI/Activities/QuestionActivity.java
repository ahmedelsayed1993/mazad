package com.aait.mazadapp.UI.Activities;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.AucationsModel;
import com.aait.mazadapp.Models.AucationsResponse;
import com.aait.mazadapp.Models.QuestionModel;
import com.aait.mazadapp.Models.QuestionResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.AuctionsAdapter;
import com.aait.mazadapp.UI.Adapters.QuestionAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    ArrayList<QuestionModel> questionModels = new ArrayList<>();
    QuestionAdapter questionAdapter;
    @Override
    protected void initializeComponents() {
        search.setVisibility(View.GONE);
        title.setText(getString(R.string.Frequently_asked_questions));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        questionAdapter = new QuestionAdapter(mContext,questionModels,R.layout.recycler_questions);
        questionAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(questionAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorAccent);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getQuestions();
            }
        });
        getQuestions();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recycler;
    }
    private void getQuestions(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getQuestion(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<QuestionResponse>() {
            @Override
            public void onResponse(Call<QuestionResponse> call, Response<QuestionResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }else {
                            questionAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }


    @Override
    public void onItemClick(View view, int position) {

    }
}

package com.aait.mazadapp.UI.Activities;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.PaginationScrollListener;
import com.aait.mazadapp.Models.AuctionDetailsModel;
import com.aait.mazadapp.Models.ChatsResponse;
import com.aait.mazadapp.Models.MessageModel;
import com.aait.mazadapp.Models.MessageResponse;
import com.aait.mazadapp.Models.RoomIDModel;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Adapters.ChatAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.google.android.gms.common.api.Api;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends ParentActivity {
    public static boolean mActive=false;
    SharedPreferences sharedPreferences;
    BroadcastReceiver MsgReciever;
    String room;
    String token;
    String name;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.et_chat)
    EditText etChat;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;


    RecyclerView.Adapter adapter;
    List<MessageModel> messageModels = new ArrayList<>();
    List<MessageModel> messageModels1 = new ArrayList<>();
    public static int reciver=0;
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage ;
    String  roomIDModel;
    String  auctionDetailsModel;
    String user;
    int lastpage;
    String id;
    LinearLayoutManager linearLayout;


    //boolean isLoading, isLastPage;
    int PAGE=0;
    int PAGE_NUM = 0;
    @Override
    protected void initializeComponents() {
        search.setVisibility(View.GONE);
        roomIDModel =  getIntent().getStringExtra("room");
        lastpage = Integer.parseInt(getIntent().getStringExtra("lastpage"));

        id = getIntent().getStringExtra("reciver");
        auctionDetailsModel =  getIntent().getStringExtra("auction");
        PAGE = lastpage;
        PAGE_NUM = lastpage;
        user = getIntent().getStringExtra("user");
        title.setText(user);
        rvRecycle.hasFixedSize();
        linearLayout = new LinearLayoutManager(this);
       linearLayout.setStackFromEnd(true);
//       linearLayout.setReverseLayout(true);
        rvRecycle.setLayoutManager(linearLayout);
        messageModels=new ArrayList<>();
        adapter = new ChatAdapter(mContext,messageModels,mSharedPrefManager.getUserData().getId());
        rvRecycle.setAdapter(adapter);
        loadMoreItems();

//        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayout) {
//            @Override
//            protected void loadMoreItems() {
//                isLoading = true;
//                currentPage += 1;
//                getChats();
//            }
//
//            @Override
//            public int getTotalPageCount() {
//                return 0;
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return isLoading;
//            }
//        });

        currentPage = lastpage;
        rvRecycle.addOnScrollListener( recyclerViewOnScrollListener );
        MsgReciever = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //  Toast.makeText(ChatActivity.this, "broad", Toast.LENGTH_SHORT).show();
                final MessageModel  chatModel1 = new MessageModel();
                chatModel1.setAvatar("");
                chatModel1.setConversation_id(0);
                chatModel1.setCreated("");
                chatModel1.setMessage("");
                chatModel1.setReceiver_id(0);
                chatModel1.setUser_id(0);
                chatModel1.setId(0);
                chatModel1.setAuction_id(0);
                if (intent.getAction().equalsIgnoreCase("new_message")){
                    //  Toast.makeText(context, "broad2", Toast.LENGTH_SHORT).show();


                    chatModel1.setMessage(intent.getStringExtra("msg"));
                    chatModel1.setId(Integer.parseInt(intent.getStringExtra("id")));
                    chatModel1.setReceiver_id(Integer.parseInt(intent.getStringExtra("receiver_id")));
                    chatModel1.setConversation_id(Integer.parseInt(intent.getStringExtra("conversation_id")));
                    chatModel1.setAvatar(intent.getStringExtra("avatar"));
                    chatModel1.setCreated(intent.getStringExtra("created"));
                    chatModel1.setUser_id(Integer.parseInt(intent.getStringExtra("user_id")));
                    chatModel1.setAuction_id(Integer.parseInt(auctionDetailsModel));

//                    Log.e("chat",chatModel1.getMessage_body());
//                    Log.e("chat",chatModel1.getIs_sender());
//                    Log.e("chat",chatModel1.getId()+"");
//                    Log.e("chat",chatModel1.getUser_id()+"");
//                    Log.e("chat",chatModel1.getType()+"");
//
//                    Log.e("chat",new Gson().toJson(chatModel1));

                    messageModels.add(chatModel1);
                    adapter = new ChatAdapter(mContext,messageModels,mSharedPrefManager.getUserData().getId());
                    rvRecycle.setAdapter(adapter);
                    // friend_id = intent.getIntExtra("user",0);
                }
            }
        };


    }
    @Override
    protected void onResume() {
        super.onResume();
//        currentPage = lastpage;
//        messageModels.clear();
//        isLastPage = false;
//        isLoading = false;
//        getChat();

        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();

        editor.putString("sender_id", roomIDModel);

        editor.apply();
        LocalBroadcastManager.getInstance(this).registerReceiver(MsgReciever,new IntentFilter("new_message"));}


    @Override
    protected void onStop() {
        super.onStop();
        SharedPreferences.Editor editor = getSharedPreferences("home", MODE_PRIVATE).edit();
        editor.putString("sender_id", "0");
        editor.apply();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_chat;
    }
    private void getChat(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);


    }
//    private void getChats() {
//        layProgress.setVisibility(View.VISIBLE);
//        layNoInternet.setVisibility(View.GONE);
//        layNoItem.setVisibility(View.GONE);
//        lay_load_more.setVisibility(View.GONE);
//        RetroWeb.getClient().create(ServiceApi.class).getChat(mLanguagePrefManager.getAppLanguage(),Integer.parseInt(roomIDModel),mSharedPrefManager.getUserData().getId(),currentPage,"ios").enqueue(new Callback<ChatsResponse>() {
//            @Override
//            public void onResponse(Call<ChatsResponse> call, Response<ChatsResponse> response) {
//                Log.e("page",currentPage+"");
//                layProgress.setVisibility(View.GONE);
//
//                if (response.isSuccessful()) {
//                    if (response.body().getKey().equals("1")) {
//                        TOTAL_PAGES = response.body().getPaginate().getTotal();
//                        if (currentPage == 1 &&response.body().getData().isEmpty()){
//                            layNoItem.setVisibility(View.VISIBLE);
//                            layNoInternet.setVisibility(View.GONE);
//                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
//                        }
//                        if (currentPage < TOTAL_PAGES) {
//                            if (response.body().getData().isEmpty()) {
//                                // reciver = response.body().getData().getData().get(0).getOther_users().get(0);
//
//                                adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
//                                rvRecycle.setAdapter(adapter);
//                            } else {
//                                for (int i=0;i<response.body().getData().size();i++){
//                                    messageModels.add(response.body().getData().get(i));
//                                }
//                                Log.e("messages",new Gson().toJson(messageModels));
//                                //reciver = response.body().getData().getData().get(0).getOther_users().get(0);
//
//                                adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
//                                rvRecycle.setAdapter(adapter);
//                            }
//                            isLoading = false;
//                        }else if (currentPage == TOTAL_PAGES){
//                            if (response.body().getData().isEmpty()) {
//
//                                // reciver = response.body().getData().getData().get(0).getOther_users().get(0);
//
//                                adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
//                                rvRecycle.setAdapter(adapter);
//                                isLastPage = true;
//                            } else {
//                                for (int i=0;i<response.body().getData().size();i++){
//                                    messageModels.add(response.body().getData().get(i));
//                                }
//                                //reciver = response.body().getData().getData().get(0).getOther_users().get(0);
//
//                                adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
//                                rvRecycle.setAdapter(adapter);
//                            }
//                        }
//                        else {
//                            isLastPage = true;
//                        }
//
//
//                    }
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ChatsResponse> call, Throwable t) {
//                Log.i("exception", "exception");
//                Log.e("errpr", String.valueOf(t));
//                CommonUtil.handleException(ChatActivity.this, t);
//                layNoInternet.setVisibility(View.VISIBLE);
//                layNoItem.setVisibility(View.GONE);
//                lay_load_more.setVisibility(View.GONE);
//                layProgress.setVisibility(View.GONE);
//                swipeRefresh.setRefreshing(false);
//            }
//        });
//
//    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @OnClick(R.id.send)
    void send() {
        final String msg = etChat.getText().toString();
        if (!msg.equals("")) {


            RetroWeb.getClient().create(ServiceApi.class).sendMessage(mLanguagePrefManager.getAppLanguage(), mSharedPrefManager.getUserData().getId(), Integer.parseInt(auctionDetailsModel),Integer.parseInt(roomIDModel), msg,id).enqueue(new Callback<MessageResponse>() {
                @Override
                public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                    if (response.isSuccessful()) {
                        if (response.body().getKey().equals("1")) {
                            MessageModel model = new MessageModel();
                            model.setMessage(response.body().getData().getMessage());
                            model.setCreated(response.body().getData().getCreated());
                            model.setAuction_id(response.body().getData().getAuction_id());
                            model.setUser_id(response.body().getData().getUser_id());
                            model.setConversation_id(response.body().getData().getConversation_id());
                            model.setAvatar(response.body().getData().getAvatar());


                            messageModels.add(response.body().getData());
                            adapter = new ChatAdapter(ChatActivity.this, messageModels, mSharedPrefManager.getUserData().getId());
                            rvRecycle.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                            etChat.setText("");
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<MessageResponse> call, Throwable t) {
                    Log.i("exception", "exception");
                    hideProgressDialog();
                    Log.e("errpr", String.valueOf(t));
                    CommonUtil.handleException(ChatActivity.this, t);
                    t.printStackTrace();
                }
            });
        }
    }


    private RecyclerView.OnScrollListener recyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int firstVisibleItemPosition = linearLayout.findFirstVisibleItemPosition();

            if (!isLoading && !isLastPage) {
                if (firstVisibleItemPosition <2) {//&& totalItemCount >= 20
                    layProgress.setVisibility(View.VISIBLE);
                    loadMoreItems();
                }
            }
        }
    };
    private void loadMoreItems() {
        isLoading = true;
        RetroWeb.getClient().create(ServiceApi.class).getChat(mLanguagePrefManager.getAppLanguage(),Integer.parseInt(roomIDModel),mSharedPrefManager.getUserData().getId()
                ,PAGE_NUM,"android").enqueue(new Callback<ChatsResponse>() {
            @Override
            public void onResponse(Call<ChatsResponse> call, Response<ChatsResponse> response) {
                isLoading = false;
                if (response.isSuccessful()) {
                    layProgress.setVisibility(View.GONE);
                    try {
                        if (response.body().getKey().equals("1")){
                            messageModels.addAll(0,response.body().getData());
                            adapter.notifyDataSetChanged();
                            if (PAGE_NUM == PAGE) { rvRecycle.scrollToPosition(linearLayout.getItemCount() - 1); }
                            else { rvRecycle.scrollToPosition(response.body().getData().size() - 1 + linearLayout.getChildCount()); }
                            PAGE_NUM--;
                            if (PAGE_NUM==0){ isLastPage = true; }

                        }else {CommonUtil.makeToast(mContext,response.body().getMsg()); }
                    }catch (Exception e) {e.printStackTrace();}
                }else {CommonUtil.makeToast(mContext,getString(R.string.connection_error)); }
            }
            @Override
            public void onFailure(Call<ChatsResponse> call, Throwable t) {
                isLoading = false;
                layProgress.setVisibility(View.GONE);
                if (PAGE_NUM==PAGE){layNoInternet.setVisibility(View.VISIBLE);}
                else {CommonUtil.makeToast(mContext,getString(R.string.connection_error));}
            }
        });










    }
}

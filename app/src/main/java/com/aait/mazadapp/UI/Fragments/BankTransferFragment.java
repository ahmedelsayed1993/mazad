package com.aait.mazadapp.UI.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aait.mazadapp.Base.BaseFragment;
import com.aait.mazadapp.Models.BanksModel;
import com.aait.mazadapp.Models.BanksResponse;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.ProfileActivity;
import com.aait.mazadapp.UI.Activities.WalletActivity;
import com.aait.mazadapp.UI.Adapters.BanksAdapter;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.aait.mazadapp.Uitls.PermissionUtils;
import com.aait.mazadapp.Uitls.ProgressRequestBody;
import com.fxn.pix.Options;
import com.fxn.pix.Pix;
import com.fxn.utility.ImageQuality;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.mazadapp.App.Constant.RequestPermission.REQUEST_IMAGES;

public class BankTransferFragment extends BaseFragment implements ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.accounts)
    RecyclerView accounts;
    LinearLayoutManager linearLayoutManager;
    BanksAdapter banksAdapter;
    ArrayList<BanksModel> banksModels = new ArrayList<>();
    @BindView(R.id.bank_name)
    EditText bank_name;
    @BindView(R.id.owner_name)
    EditText owner_name;
    @BindView(R.id.account_number)
    EditText account_number;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.image)
    TextView image;
    private String ImageBasePath = null;
    ArrayList<String> returnValue = new ArrayList<>();
    Options options = Options.init()
            .setRequestCode(100)                                                 //Request code for activity results
            .setCount(1)                                                         //Number of images to restict selection count
            .setFrontfacing(false)                                                //Front Facing camera on start
            .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
            .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
            .setPath("/pix/images");
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_bank_transfer;
    }

    @Override
    protected void initializeComponents(View view) {
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        banksAdapter = new BanksAdapter(mContext,banksModels,R.layout.recycler_banks);
        accounts.setLayoutManager(linearLayoutManager);
        accounts.setAdapter(banksAdapter);
        getbanks();
    }
    private void getbanks(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getBanks(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<BanksResponse>() {
            @Override
            public void onResponse(Call<BanksResponse> call, Response<BanksResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        if (response.body().getData().size()==0){

                        }else {
                            banksAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BanksResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, options);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, options);
        }
    }
    @OnClick(R.id.charge)
    void onCharge(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,bank_name,getString(R.string.bank_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,owner_name,getString(R.string.account_owner_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,account_number,getString(R.string.account_number))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,amount,getString(R.string.amount_payed))||
        CommonUtil.checkTextError(image,getString(R.string.bank_transfer_image))){
            return;
        }else {
            transfer(ImageBasePath);
        }
    }
    private void transfer(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, BankTransferFragment.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).transfer(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()
        ,owner_name.getText().toString(),bank_name.getText().toString(),amount.getText().toString(),account_number.getText().toString(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext, WalletActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @OnClick(R.id.image)
    void onAvatar(){
        getPickImageWithPermission();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == 100) {
                returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath = returnValue.get(0);
                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
                image.setText(ImageBasePath);
                if (ImageBasePath!=null) {

                }
            }
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}

package com.aait.mazadapp.UI.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Fragments.ActiveFragment;
import com.aait.mazadapp.UI.Fragments.BankTransferFragment;
import com.aait.mazadapp.UI.Fragments.CheckFragment;
import com.aait.mazadapp.UI.Fragments.CommingFragment;
import com.aait.mazadapp.UI.Fragments.CreditFragment;

public class WalletTapAdapter extends FragmentPagerAdapter {

    private Context context;

    public WalletTapAdapter(Context context , FragmentManager fm) {
        super(fm);
        this.context = context;

    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new BankTransferFragment();
        }else if (position == 1){
            return new CreditFragment();
        }else {
            return new CheckFragment();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.Bank_transfer);
        }else if (position == 1){
            return context.getString(R.string.Credit_card);
        }else {
            return context.getString(R.string.check);
        }
    }
}


package com.aait.mazadapp.UI.Adapters;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Fragments.ActiveFragment;
import com.aait.mazadapp.UI.Fragments.CommingFragment;


public class SubscribeTapAdapter extends FragmentPagerAdapter {

    private Context context;
    CategoriesModel categoriesModel;

    public SubscribeTapAdapter(Context context , FragmentManager fm,CategoriesModel categoriesModel) {
        super(fm);
        this.context = context;
        this.categoriesModel = categoriesModel;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new ActiveFragment().newInstance(categoriesModel);
        }else {
            return new CommingFragment().newInstance(categoriesModel);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.Active_auctions);
        }else {
            return context.getString(R.string.Upcoming_auctions);
        }
    }
}

package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aait.mazadapp.App.Constant;
import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Models.UserResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditDataActivity extends ParentActivity {
    @BindView(R.id.lay_no_internet)
    RelativeLayout lay_no_internet;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.name)
    EditText name;
    String mAdresse="", mLang=null, mLat = null, mAddress="",Address = "";
    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.edit_data));
        search.setVisibility(View.GONE);
        getProfile();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_edit_data;
    }
    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getUser(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId()).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                lay_no_internet.setVisibility(View.GONE);
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                        name.setText(response.body().getData().getName());
                        address.setText(response.body().getData().getAddress());
                        phone.setText(response.body().getData().getPhone());
                        mLat = response.body().getData().getLat();
                        mLang= response.body().getData().getLng();
                        mAddress = response.body().getData().getAddress();


                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                lay_no_internet.setVisibility(View.VISIBLE);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void edit(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).edit(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getId(),name.getText().toString(),phone.getText().toString(),mLat,mLang,mAddress).enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){

                        name.setText(response.body().getData().getName());
                        address.setText(response.body().getData().getAddress());
                        phone.setText(response.body().getData().getPhone());
                        mLat = response.body().getData().getLat();
                        mLang= response.body().getData().getLng();
                        mAddress = response.body().getData().getAddress();


                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }

            }

            @Override
            public void onFailure(Call<UserResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
    @OnClick(R.id.save)
    void onSave(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,name,getString(R.string.name))||
                CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.phone_number))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                CommonUtil.checkTextError(address,getString(R.string.address))){
            return;
        }else {
            edit();
        }
    }
    @OnClick(R.id.address)
    void onAddress(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.RequestCode.GET_LOCATION) {
            if (resultCode == RESULT_OK) {
                mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                mAddress = data.getStringExtra("LOCATION");
                Address = data.getStringExtra("LOC");
                mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse + "  " + mAddress + "  " + address);
                if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                    address.setText(mAddress);
                } else if (mLanguagePrefManager.getAppLanguage().equals("en")) {
                    address.setText(mAdresse);
                }
            }

        }
    }
}

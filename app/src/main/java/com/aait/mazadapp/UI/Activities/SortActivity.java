package com.aait.mazadapp.UI.Activities;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.aait.mazadapp.Base.ParentActivity;
import com.aait.mazadapp.Listeners.OnItemClickListener;
import com.aait.mazadapp.Models.CategoriesModel;
import com.aait.mazadapp.Models.ListModel;
import com.aait.mazadapp.Models.ListResponse;
import com.aait.mazadapp.Network.RetroWeb;
import com.aait.mazadapp.Network.ServiceApi;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Views.ListDialog;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SortActivity extends ParentActivity implements OnItemClickListener {
    @BindView(R.id.title)
    TextView title;
    @OnClick(R.id.back)
    void onBack(){
        onBackPressed();
    }
    CategoriesModel categoriesModel;
    String special=null;
    String popular = null;
    String completion = null;
    String day = null;
    String three_days = null;
    String week = null;
    String city_id =null;
    ArrayList<ListModel> cities = new ArrayList<>();
    ListDialog listDialog;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.Featured_ads)
    RadioButton Featured_ads;
    @BindView(R.id.Most_popular_ads)
    RadioButton Most_popular_ads;
    @BindView(R.id.upcoming_auctions)
    RadioButton upcoming_auctions;
    @BindView(R.id.ending_auctions)
    RadioButton ending_auctions;
    @BindView(R.id.since_24)
    RadioButton since_24;
    @BindView(R.id.since_3)
    RadioButton since_3;
    @BindView(R.id.since_week)
    RadioButton since_week;


    @Override
    protected void initializeComponents() {
        title.setText(getString(R.string.sort_by));
        categoriesModel = (CategoriesModel)getIntent().getSerializableExtra("category");
        Log.e("ctegory",new Gson().toJson(categoriesModel));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_sort;
    }
    @OnClick(R.id.upcoming_auctions)
    void onComing(){
        Intent intent = new Intent(mContext,AucationsActivity.class);
        intent.putExtra("category",categoriesModel);
        Log.e("catego",new Gson().toJson(categoriesModel));
        intent.putExtra("type","up");
        startActivity(intent);
    }
    @OnClick(R.id.location)
    void onLocation(){
        getCities();
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCities(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<ListResponse>() {
            @Override
            public void onResponse(Call<ListResponse> call, Response<ListResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getKey().equals("1")){
                        cities = response.body().getData();
                        listDialog = new ListDialog(mContext,SortActivity.this,cities,getString(R.string.City));
                        listDialog.show();

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ListResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.reset)
    void onReset(){
        special = null;
        popular = null;
        completion = null;
        day = null;
        three_days= null;
        week=null;
        city.setText(getString(R.string.auction_ads));
        since_3.setChecked(false);
        since_24.setChecked(false);
        since_week.setChecked(false);
        Featured_ads.setChecked(false);
        upcoming_auctions.setChecked(false);
        Most_popular_ads.setChecked(false);
        ending_auctions.setChecked(false);
    }
    @OnClick(R.id.Featured_ads)
    void onFeatured(){
        special = "1";
         popular = null;
        completion = null;
         day = null;
         three_days = null;
         week = null;
    }
    @OnClick(R.id.Most_popular_ads)
    void onPopular(){
        special = null;
        popular = "1";
        completion = null;
        day = null;
        three_days = null;
        week = null;
    }
    @OnClick(R.id.ending_auctions)
    void onEnding(){
        special = null;
        popular = null;
        completion = "1";
        day = null;
        three_days = null;
        week = null;
    }
    @OnClick(R.id.since_24)
    void on24(){
        special = null;
        popular = null;
        completion = null;
        day = "1";
        three_days = null;
        week = null;
    }
    @OnClick(R.id.since_3)
    void on3(){
        special = null;
        popular = null;
        completion = null;
        day = null;
        three_days = "1";
        week = null;
    }
    @OnClick(R.id.since_week)
    void onWeek(){
        special = null;
        popular = null;
        completion = null;
        day = null;
        three_days = null;
        week = "1";
    }
    @OnClick(R.id.done)
    void onDone(){
        Intent intentData = new Intent();
        intentData.putExtra("special", special);
        intentData.putExtra("popular", popular);
        intentData.putExtra("completion", completion);
        intentData.putExtra("day", day);
        intentData.putExtra("three_day", three_days);

        intentData.putExtra("week",week);
        intentData.putExtra("city_id",city_id);
        setResult(RESULT_OK, intentData);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        city_id = cities.get(position).getId();
        city.setText(cities.get(position).getName());


    }
}

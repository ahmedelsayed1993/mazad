package com.aait.mazadapp.Listeners;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public interface PaginationAdapterCallback {
    void retryPageLoad();
}

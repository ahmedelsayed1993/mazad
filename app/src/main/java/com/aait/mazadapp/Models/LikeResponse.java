package com.aait.mazadapp.Models;

public class LikeResponse extends BaseResponse{
    private String isLike;

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }
}

package com.aait.mazadapp.Models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String key;
    private String msg;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    public boolean issucessfull(){
        if (key.equals("1")){
            return true;
        }else {
            return false;
        }
    }
}

package com.aait.mazadapp.Models;

public class UserResponse extends BaseResponse {
    private UserModel data;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }
}

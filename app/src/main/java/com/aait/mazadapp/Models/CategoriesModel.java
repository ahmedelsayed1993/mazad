package com.aait.mazadapp.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class CategoriesModel implements Serializable {
    private int id;
    private String name;
    private String image;

    private String category_key;

    private int auctions_count;
    private String color;
    private String background_color;
    private ArrayList<String> filters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    public String getCategory_key() {
        return category_key;
    }

    public void setCategory_key(String category_key) {
        this.category_key = category_key;
    }



    public int getAuctions_count() {
        return auctions_count;
    }

    public void setAuctions_count(int auctions_count) {
        this.auctions_count = auctions_count;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getBackground_color() {
        return background_color;
    }

    public void setBackground_color(String background_color) {
        this.background_color = background_color;
    }

    public ArrayList<String> getFilters() {
        return filters;
    }

    public void setFilters(ArrayList<String> filters) {
        this.filters = filters;
    }
}

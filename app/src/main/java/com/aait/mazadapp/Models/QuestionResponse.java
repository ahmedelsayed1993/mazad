package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class QuestionResponse extends BaseResponse {
    private ArrayList<QuestionModel> data;

    public ArrayList<QuestionModel> getData() {
        return data;
    }

    public void setData(ArrayList<QuestionModel> data) {
        this.data = data;
    }
}

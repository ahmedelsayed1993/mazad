package com.aait.mazadapp.Models;

import java.io.Serializable;

public class AucationsModel implements Serializable {
    private int id;
    private String category;
    private String title;
    private String image;
    private String last_price;
    private String date;
    private Long end_date;
    private Long start_date;
    private String owner_image;
    private int tendersCount;
    private String owner_name;
    private int show_counter;

    public int getShow_counter() {
        return show_counter;
    }

    public void setShow_counter(int show_counter) {
        this.show_counter = show_counter;
    }

    public Long getStart_date() {
        return start_date;
    }

    public void setStart_date(Long start_date) {
        this.start_date = start_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLast_price() {
        return last_price;
    }

    public void setLast_price(String last_price) {
        this.last_price = last_price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Long end_date) {
        this.end_date = end_date;
    }

    public String getOwner_image() {
        return owner_image;
    }

    public void setOwner_image(String owner_image) {
        this.owner_image = owner_image;
    }

    public int getTendersCount() {
        return tendersCount;
    }

    public void setTendersCount(int tendersCount) {
        this.tendersCount = tendersCount;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }
}

package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class CheckResponse extends BaseResponse{
    private ArrayList<CheckModel> data;

    public ArrayList<CheckModel> getData() {
        return data;
    }

    public void setData(ArrayList<CheckModel> data) {
        this.data = data;
    }
}

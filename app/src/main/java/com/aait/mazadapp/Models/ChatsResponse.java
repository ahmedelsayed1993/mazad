package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class ChatsResponse extends BaseResponse{
    private PaginationModel paginate;
    private ArrayList<MessageModel> data;

    public PaginationModel getPaginate() {
        return paginate;
    }

    public void setPaginate(PaginationModel paginate) {
        this.paginate = paginate;
    }

    public ArrayList<MessageModel> getData() {
        return data;
    }

    public void setData(ArrayList<MessageModel> data) {
        this.data = data;
    }
}

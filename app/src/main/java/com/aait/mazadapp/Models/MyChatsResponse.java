package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class MyChatsResponse extends BaseResponse{
  private ArrayList<ChatsModel> data;

    public ArrayList<ChatsModel> getData() {
        return data;
    }

    public void setData(ArrayList<ChatsModel> data) {
        this.data = data;
    }
}

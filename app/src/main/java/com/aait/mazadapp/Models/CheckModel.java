package com.aait.mazadapp.Models;

import java.io.Serializable;

public class CheckModel implements Serializable {
    private int id;
    private String branch;
    private String lat;
    private String lng;
    private String day_from;
    private String day_to;
    private String time_from;
    private String time_to;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDay_from() {
        return day_from;
    }

    public void setDay_from(String day_from) {
        this.day_from = day_from;
    }

    public String getDay_to() {
        return day_to;
    }

    public void setDay_to(String day_to) {
        this.day_to = day_to;
    }

    public String getTime_from() {
        return time_from;
    }

    public void setTime_from(String time_from) {
        this.time_from = time_from;
    }

    public String getTime_to() {
        return time_to;
    }

    public void setTime_to(String time_to) {
        this.time_to = time_to;
    }
}

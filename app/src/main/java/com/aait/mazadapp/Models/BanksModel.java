package com.aait.mazadapp.Models;

import java.io.Serializable;

public class BanksModel implements Serializable {
    private int id;
    private String bank_name;
    private String account_name;
    private String account_number;
    private String iban_number;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getIban_number() {
        return iban_number;
    }

    public void setIban_number(String iban_number) {
        this.iban_number = iban_number;
    }
}

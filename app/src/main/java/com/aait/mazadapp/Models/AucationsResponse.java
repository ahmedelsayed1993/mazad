package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class AucationsResponse extends BaseResponse{
   private ArrayList<AucationsModel> data;

    public ArrayList<AucationsModel> getData() {
        return data;
    }

    public void setData(ArrayList<AucationsModel> data) {
        this.data = data;
    }
}

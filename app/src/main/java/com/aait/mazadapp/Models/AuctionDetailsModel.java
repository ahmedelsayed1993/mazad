package com.aait.mazadapp.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class AuctionDetailsModel implements Serializable {
    private ArrayList<ImageModel> images;
    private String image_3D;
    private String title;
    private String start_date;
    private String starting_price;
    private String last_price;
    private String lat;
    private String lng;
    private String category_key;
    private String description;
    private int id;
    private int category_id;
    private String city_id;
    private String region_id;
    private String plate_number;
    private String property_type;
    private String property_area;
    private String  vehicle_id;
    private String  brand_id;
    private String  model_id;
    private String  year_make;
    private String  count_distances;
    private String  count_plate_numbers;
    private String advertiser_name;
    private String advertiser_phone;
    private String advertiser_avatar;
    private String amount_insurance;
    private int advertiser_id;
    private int show_counter;

    public int getShow_counter() {
        return show_counter;
    }

    public void setShow_counter(int show_counter) {
        this.show_counter = show_counter;
    }

    private String link_share;
    private int subscribed;
    private long end_date;
    private int isLike;
    private String end_date_hijri;
    private ArrayList<TendersModel> tenders;

    public int getIsLike() {
        return isLike;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public void setIsLike(int isLike) {
        this.isLike = isLike;
    }

    public String getEnd_date_hijri() {
        return end_date_hijri;
    }

    public void setEnd_date_hijri(String end_date_hijri) {
        this.end_date_hijri = end_date_hijri;
    }

    public String getCategory_key() {
        return category_key;
    }

    public void setCategory_key(String category_key) {
        this.category_key = category_key;
    }

    public int getAdvertiser_id() {
        return advertiser_id;
    }

    public void setAdvertiser_id(int advertiser_id) {
        this.advertiser_id = advertiser_id;
    }

    public String getLink_share() {
        return link_share;
    }

    public void setLink_share(String link_share) {
        this.link_share = link_share;
    }

    public ArrayList<TendersModel> getTenders() {
        return tenders;
    }

    public void setTenders(ArrayList<TendersModel> tenders) {
        this.tenders = tenders;
    }

    public long getEnd_date() {
        return end_date;
    }

    public void setEnd_date(long end_date) {
        this.end_date = end_date;
    }

    public ArrayList<ImageModel> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageModel> images) {
        this.images = images;
    }

    public String getImage_3D() {
        return image_3D;
    }

    public void setImage_3D(String image_3D) {
        this.image_3D = image_3D;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getStarting_price() {
        return starting_price;
    }

    public void setStarting_price(String starting_price) {
        this.starting_price = starting_price;
    }

    public String getLast_price() {
        return last_price;
    }

    public void setLast_price(String last_price) {
        this.last_price = last_price;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getRegion_id() {
        return region_id;
    }

    public void setRegion_id(String region_id) {
        this.region_id = region_id;
    }

    public String getPlate_number() {
        return plate_number;
    }

    public void setPlate_number(String plate_number) {
        this.plate_number = plate_number;
    }

    public String getProperty_type() {
        return property_type;
    }

    public void setProperty_type(String property_type) {
        this.property_type = property_type;
    }

    public String getProperty_area() {
        return property_area;
    }

    public void setProperty_area(String property_area) {
        this.property_area = property_area;
    }

    public String  getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String  vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String  getBrand_id() {
        return brand_id;
    }

    public void setBrand_id(String  brand_id) {
        this.brand_id = brand_id;
    }

    public String  getModel_id() {
        return model_id;
    }

    public void setModel_id(String  model_id) {
        this.model_id = model_id;
    }

    public String  getYear_make() {
        return year_make;
    }

    public void setYear_make(String  year_make) {
        this.year_make = year_make;
    }

    public String  getCount_distances() {
        return count_distances;
    }

    public void setCount_distances(String  count_distances) {
        this.count_distances = count_distances;
    }

    public String  getCount_plate_numbers() {
        return count_plate_numbers;
    }

    public void setCount_plate_numbers(String  count_plate_numbers) {
        this.count_plate_numbers = count_plate_numbers;
    }

    public String getAdvertiser_name() {
        return advertiser_name;
    }

    public void setAdvertiser_name(String advertiser_name) {
        this.advertiser_name = advertiser_name;
    }

    public String getAdvertiser_phone() {
        return advertiser_phone;
    }

    public void setAdvertiser_phone(String advertiser_phone) {
        this.advertiser_phone = advertiser_phone;
    }

    public String getAdvertiser_avatar() {
        return advertiser_avatar;
    }

    public void setAdvertiser_avatar(String advertiser_avatar) {
        this.advertiser_avatar = advertiser_avatar;
    }

    public String getAmount_insurance() {
        return amount_insurance;
    }

    public void setAmount_insurance(String amount_insurance) {
        this.amount_insurance = amount_insurance;
    }

    public int getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(int subscribed) {
        this.subscribed = subscribed;
    }
}

package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class SugestionResponse extends BaseResponse{
   private PhonesModel phones;
   private ArrayList<SocialModel> data;

    public PhonesModel getPhones() {
        return phones;
    }

    public void setPhones(PhonesModel phones) {
        this.phones = phones;
    }

    public ArrayList<SocialModel> getData() {
        return data;
    }

    public void setData(ArrayList<SocialModel> data) {
        this.data = data;
    }
}

package com.aait.mazadapp.Models;

import java.util.ArrayList;

public class CategoriesResponse extends BaseResponse {
    private ArrayList<CategoriesModel> data;
    ArrayList<ImageModel> images;

    public ArrayList<ImageModel> getImages() {
        return images;
    }

    public void setImages(ArrayList<ImageModel> images) {
        this.images = images;
    }

    public ArrayList<CategoriesModel> getData() {
        return data;
    }

    public void setData(ArrayList<CategoriesModel> data) {
        this.data = data;
    }
}

package com.aait.mazadapp.Models;

import java.io.Serializable;

public class ListModel implements Serializable {
    private String id;
    private String name;
    private boolean checked;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public ListModel(String id, String name) {
        this.id = id;
        this.name= name;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

package com.aait.mazadapp.Models;

import java.io.Serializable;

public class ImageModel implements Serializable {
    private int id;
    private String image;
    private String link;


    public ImageModel(int id, String image) {
        this.id = id;
        this.image = image;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

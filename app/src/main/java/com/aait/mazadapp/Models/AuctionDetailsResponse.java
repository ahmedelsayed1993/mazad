package com.aait.mazadapp.Models;

public class AuctionDetailsResponse extends BaseResponse {
    private AuctionDetailsModel data;

    public AuctionDetailsModel getData() {
        return data;
    }

    public void setData(AuctionDetailsModel data) {
        this.data = data;
    }
}

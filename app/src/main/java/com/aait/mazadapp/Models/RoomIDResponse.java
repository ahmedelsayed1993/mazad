package com.aait.mazadapp.Models;

public class RoomIDResponse extends BaseResponse{
    private RoomIDModel data;

    public RoomIDModel getData() {
        return data;
    }

    public void setData(RoomIDModel data) {
        this.data = data;
    }
}

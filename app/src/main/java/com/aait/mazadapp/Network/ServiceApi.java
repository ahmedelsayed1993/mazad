package com.aait.mazadapp.Network;

import com.aait.mazadapp.Models.AboutResponse;
import com.aait.mazadapp.Models.AucationsResponse;
import com.aait.mazadapp.Models.AuctionDetailsResponse;
import com.aait.mazadapp.Models.BanksResponse;
import com.aait.mazadapp.Models.BaseResponse;
import com.aait.mazadapp.Models.CallUsResponse;
import com.aait.mazadapp.Models.CategoriesResponse;
import com.aait.mazadapp.Models.ChatsResponse;
import com.aait.mazadapp.Models.CheckCodeResponse;
import com.aait.mazadapp.Models.CheckResponse;
import com.aait.mazadapp.Models.LikeResponse;
import com.aait.mazadapp.Models.ListResponse;
import com.aait.mazadapp.Models.MessageResponse;
import com.aait.mazadapp.Models.MyChatsResponse;
import com.aait.mazadapp.Models.NotificationResponse;
import com.aait.mazadapp.Models.QuestionResponse;
import com.aait.mazadapp.Models.ResedResponse;
import com.aait.mazadapp.Models.RoomIDResponse;
import com.aait.mazadapp.Models.SubScripResponse;
import com.aait.mazadapp.Models.SugestionResponse;
import com.aait.mazadapp.Models.UserResponse;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {
    @Multipart
    @POST(Urls.SignUp)
    Call<UserResponse> register(@Query("lang") String lang,
                                @Query("name") String name,
                                @Query("phone") String phone,
                                @Query("lat") String lat,
                                @Query("lng") String lng,
                                @Query("address") String address,
                                @Query("email") String email,
                                @Query("gender") String gender,
                                @Query("idNumber") String idNumber,
                                @Query("device_id") String device_id,
                                @Query("device_type") String device_type,
                                @Query("password") String password,
                                @Part MultipartBody.Part copy_residence);

    @POST(Urls.CheckCode)
    Call<CheckCodeResponse> check(@Query("lang") String lang,
                                  @Query("user_id") int user_id,
                                  @Query("code") String code);
    @POST(Urls.ResendCode)
    Call<ResedResponse> resend (@Query("lang") String lang,
                                @Query("user_id") int user_id);

    @POST(Urls.ForgotPass)
    Call<UserResponse> forgotPass(@Query("lang") String lang,
                                  @Query("phone") String phone);
    @POST(Urls.UpdatePassword)
    Call<UserResponse> updatePass(@Query("lang") String lang,
                                  @Query("user_id") int user_id,
                                  @Query("password") String password,
                                  @Query("code") String code);
    @POST(Urls.SignIn)
    Call<UserResponse> login(@Query("lang") String lang,
                             @Query("phone") String phone,
                             @Query("password") String password,
                             @Query("device_id") String device_id,
                             @Query("device_type") String device_type);
    @POST(Urls.Categories)
    Call<CategoriesResponse> getCats(@Query("lang") String lang);
    @POST(Urls.Auctions)
    Call<AucationsResponse> getAuctions(@Query("lang") String lang,
                                        @Query("category_id") int category_id,
                                        @Query("active") int active);
    @POST(Urls.AuctionDetails)
    Call<AuctionDetailsResponse> getAuction(@Query("lang") String lang,
                                            @Query("auction_id") int auction_id,
                                            @Query("user_id") String  user_id);

    @POST(Urls.Subscription)
    Call<SubScripResponse> subscrip(@Query("lang") String lang,
                                    @Query("user_id") int user_id,
                                    @Query("auction_id") int auction_id);
    @POST(Urls.Tender)
    Call<SubScripResponse> tender(@Query("lang") String lang,
                                  @Query("user_id") int user_id,
                                  @Query("auction_id") int auction_id,
                                  @Query("price") String price);
    @POST(Urls.Financial)
    Call<SubScripResponse> financial(@Query("lang") String lang,
                                  @Query("user_id") int user_id);
    @POST(Urls.EditProfile)
    Call<UserResponse> getUser(@Query("lang") String lang,
                               @Query("user_id") int user_id);
    @POST(Urls.EditProfile)
    Call<UserResponse> edit(@Query("lang") String lang,
                            @Query("user_id") int user_id,
                            @Query("name") String name,
                            @Query("phone") String phone,
                            @Query("lat") String lat,
                            @Query("lng") String lng,
                            @Query("address") String address);
    @Multipart
    @POST(Urls.EditProfile)
    Call<UserResponse> updateImage(@Query("lang") String lang,
                                   @Query("user_id") int user_id,
                                   @Part MultipartBody.Part avatar);
    @POST(Urls.Cities)
    Call<ListResponse> getCities(@Query("lang") String lang);
    @POST(Urls.Property_area)
    Call<ListResponse> getAreas(@Query("lang") String lang,
                                @Query("category_id") int category_id);
    @POST(Urls.Vehicles)
    Call<ListResponse> getVehicles(@Query("lang") String lang);
    @POST(Urls.Brands)
    Call<ListResponse> getBrands(@Query("lang") String lang,
                                 @Query("vehicle_id") String vehicle_id);
    @POST(Urls.Models)
    Call<ListResponse> getModel(@Query("lang") String lang,
                                 @Query("brand_id") String brand_id);
    @POST(Urls.Advertisers)
    Call<ListResponse> getAdver(@Query("lang") String lang,
                                @Query("category_id") String category_id);
    @POST(Urls.Count)
    Call<ListResponse> getCount(@Query("lang") String lang,
                                @Query("category_id") String category_id);
    @POST(Urls.Region)
    Call<ListResponse> getRegions(@Query("lang") String lang);
    @POST(Urls.Converstion_id)
    Call<RoomIDResponse> getRoom(@Query("lang") String lang,
                                 @Query("user_id") int user_id,
                                 @Query("auction_id") int auction_id);
    @POST(Urls.SendMessage)
    Call<MessageResponse> sendMessage(@Query("lang") String lang,
                                      @Query("user_id") int user_id,
                                      @Query("auction_id") int auction_id,
                                      @Query("conversation_id") int conversation_id,
                                      @Query("message") String message,
                                      @Query("receiver_id") String receiver_id);
    @POST(Urls.Conversation)
    Call<ChatsResponse> getChat(@Query("lang") String lang,
                                @Query("conversation_id") int conversation_id,
                                @Query("user_id") int user_id,
                                @Query("page") int page,
                                @Query("app_type") String app_type);
    @POST(Urls.MyChats)
    Call<MyChatsResponse> getMyChats(@Query("lang") String lang,
                                     @Query("user_id") int user_id);
    @POST(Urls.About)
    Call<AboutResponse> getAbout(@Query("lang") String lang);
    @POST(Urls.Terms)
    Call<AboutResponse> getTerms(@Query("lang") String lang);
    @POST(Urls.CallUs)
    Call<CallUsResponse> callUs(@Query("lang") String lang);
    @POST(Urls.Like)
    Call<LikeResponse> like(@Query("lang") String lang,
                            @Query("user_id") int user_id,
                            @Query("auction_id") int auction_id);
    @POST(Urls.MyLikes)
    Call<AucationsResponse> getLikes(@Query("lang") String lang,
                                     @Query("user_id") int user_id);
    @POST(Urls.Subcriptions)
    Call<AucationsResponse> getSubcriptions(@Query("lang") String lang,
                                     @Query("user_id") int user_id);
    @POST(Urls.Winner)
    Call<AucationsResponse> getWinner(@Query("lang") String lang,
                                      @Query("user_id") int user_id);
    @POST(Urls.Question)
    Call<QuestionResponse> getQuestion(@Query("lang") String lang);
    @POST(Urls.Suggestion)
    Call<SugestionResponse> suggestion(@Query("lang") String lang,
                                       @Query("name") String name,
                                       @Query("subject") String subject,
                                       @Query("message") String message);
    @POST(Urls.Suggestion)
    Call<BaseResponse> writesuggestion(@Query("lang") String lang,
                                       @Query("name") String name,
                                       @Query("subject") String subject,
                                       @Query("message") String message);

    @POST(Urls.ResetPassword)
    Call<BaseResponse> resetPass(@Query("lang") String lang,
                                 @Query("user_id") int user_id,
                                 @Query("current_password") String current_password,
                                 @Query("password") String password);

    @POST(Urls.LogOut)
    Call<BaseResponse> logout(@Query("lang") String lang,
                              @Query("user_id") int user_id,
                              @Query("device_id") String device_id,
                              @Query("device_type") String device_type);
    @POST(Urls.Search)
    Call<AucationsResponse> search(@Query("lang") String lang,
                                   @Query("text") String text);
    @POST(Urls.SearchByCategory)
    Call<AucationsResponse> searchByCat(@Query("lang") String lang,
                                        @Query("text") String text,
                                        @Query("category_id") int category_id);
    @POST(Urls.Notification)
    Call<NotificationResponse> getNotification(@Query("lang") String lang,
                                               @Query("user_id") int user_id);
    @POST(Urls.Banks)
    Call<BanksResponse> getBanks(@Query("lang") String lang,
                                 @Query("user_id") int user_id);
    @Multipart
    @POST(Urls.Banks)
    Call<BaseResponse> transfer(@Query("lang") String lang,
                                @Query("user_id") int user_id,
                                @Query("name") String name,
                                @Query("bank_name") String bank_name,
                                @Query("amount") String amount,
                                @Query("account_number") String account_number,
                                @Part MultipartBody.Part image);
    @POST(Urls.Check)
    Call<CheckResponse> check(@Query("lang") String lang);

    @POST(Urls.Filter)
    Call<AucationsResponse> filter(@Query("lang") String lang,
                                   @Query("category_id") int category_id,
                                   @Query("region_id") String region_id,
                                   @Query("city_id") String city_id,
                                   @Query("countPlateNumbers") String countPlateNumbers,
                                   @Query("property_type") String property_type,
                                   @Query("property_area") String property_area,
                                   @Query("vehicle_id") String vehicle_id,
                                   @Query("brand_id") String brand_id,
                                   @Query("model_id") String model_id,
                                   @Query("year_make") String year_make,
                                   @Query("min_price") String min_price,
                                   @Query("max_price") String max_price,
                                   @Query("user_id") String user_id,
                                   @Query("count_distances") String count_distances,
                                   @Query("active") int active);
    @POST(Urls.Sort)
    Call<AucationsResponse> sort(@Query("lang") String lang,
                                 @Query("category_id") int category_id,
                                 @Query("city_id") String city_id,
                                 @Query("special") String special,
                                 @Query("popular") String popular,
                                 @Query("completion") String completion,
                                 @Query("day") String day,
                                 @Query("three_days") String three_days,
                                 @Query("week") String week);
    @POST(Urls.DeleteNotification)
    Call<BaseResponse> delete(@Query("lang") String lang,
                              @Query("notification_id") int notification_id,
                              @Query("user_id") int user_id);
}

package com.aait.mazadapp.Network;

public class Urls {
    public final static String BaseUrl = "https://mazadat.aait-sa.com/api/";

    public final static String SignUp = "sign-up";
    public final static String CheckCode = "check-code";
    public final static String ResendCode = "resend-code";
    public final static String ForgotPass = "forget-password";
    public final static String UpdatePassword = "update-password";
    public final static String SignIn = "sign-in";
    public final static String Categories = "categories";
    public final static String Auctions = "auctions";
    public final static String AuctionDetails = "details-auction";
    public final static String Subscription = "subscription";
    public final static String Tender = "tender";
    public final static String Financial = "financial";
    public final static String EditProfile = "edit-profile";
    public final static String Cities = "cities";
    public final static String Vehicles = "vehicles";
    public final static String Brands = "brands";
    public final static String Models = "models";
    public final static String Advertisers = "advertisers";
    public final static String Count = "count-distances";
    public final static String Region = "regions";
    public final static String Converstion_id = "conversation-id";
    public final static String SendMessage = "send-message";
    public final static String Conversation = "conversation";
    public final static String MyChats = "my-conversations";
    public final static String About = "about";
    public final static String Terms = "terms";
    public final static String CallUs = "call-us";
    public final static String Like = "like";
    public final static String MyLikes = "user-likes";
    public final static String Subcriptions = "subscriptions-auctions";
    public final static String Question = "questions";
    public final static String Suggestion = "complaints";
    public final static String Winner = "winner";
    public final static String ResetPassword = "reset-password";
    public final static String LogOut = "log-out";
    public final static String Search = "search";
    public final static String SearchByCategory = "category-search";
    public final static String Notification  = "notifications";
    public final static String Banks = "bank-transfer";
    public final static String Check = "check";
    public final static String Filter = "filter";
    public final static String Property_area = "property-areas";
    public final static String Sort = "sort";
    public final static String DeleteNotification = "delete-notification";

}

package com.aait.mazadapp.FCM;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aait.mazadapp.Pereferences.LanguagePrefManager;
import com.aait.mazadapp.Pereferences.SharedPrefManager;
import com.aait.mazadapp.R;
import com.aait.mazadapp.UI.Activities.AuctionDetailsActivity;
import com.aait.mazadapp.UI.Activities.ComingDetailsActivity;
import com.aait.mazadapp.UI.Activities.MainActivity;
import com.aait.mazadapp.UI.Activities.NotificationActivity;
import com.aait.mazadapp.Uitls.CommonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

//import com.aait.mazadapp.UI.Activities.MainActivity;
//import com.aait.mazadapp.UI.Activities.NotificationActivity;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPreferences sharedPreferences;
    private static final String TAG = "FCM Messaging";

    String notificationTitle;

    String notificationMessage;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

    // OrderModel mOrderModel;

    LanguagePrefManager mLanguagePrefManager;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);
        CommonUtil.onPrintLog(remoteMessage.getNotification());
        CommonUtil.onPrintLog(remoteMessage.getData());
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePrefManager(getApplicationContext());

//        if (!mSharedPrefManager.getUserData().getNotification_status().equals("")) {
//            return;
//        }


//        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.getData().get("title");
        notificationMessage = remoteMessage.getData().get("body");
        notificationType = remoteMessage.getData().get("type");
        // if the notification contains data payload
        if (remoteMessage == null) {
            return;
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.getLoginStatus()) {
            return;
        } else {
            if (remoteMessage.getData().get("type").equals("notification")){
                if (remoteMessage.getData().get("status").equals("1")) {
                    Intent intent = new Intent(this, AuctionDetailsActivity.class);
                    intent.putExtra("category", remoteMessage.getData().get("category_key"));
                    intent.putExtra("auction", remoteMessage.getData().get("auction_id"));
//                startActivity(intent);
                    showNotification(remoteMessage, intent);
                }else if (remoteMessage.getData().get("status").equals("0")){
                    Intent intent = new Intent(this, ComingDetailsActivity.class);
                    intent.putExtra("category", remoteMessage.getData().get("category_key"));
                    intent.putExtra("auction", remoteMessage.getData().get("auction_id"));
//                startActivity(intent);
                    showNotification(remoteMessage, intent);
                }
            }else if (remoteMessage.getData().get("type").equals("public_notification")) {
                Intent intent = new Intent(this, NotificationActivity.class);
                showNotification(remoteMessage, intent);
            }else if (remoteMessage.getData().get("type").equals("chat")){
            if (sharedPreferences.getString("sender_id", "0").equals(remoteMessage.getData().get("conversation_id"))) {
                Intent intent = new Intent("new_message");
                intent.putExtra("msg", remoteMessage.getData().get("message"));
                Log.e("msg",remoteMessage.getData().get("message"));

                intent.putExtra("receiver_id", remoteMessage.getData().get("receiver_id"));

                intent.putExtra("avatar", remoteMessage.getData().get("avatar"));

                intent.putExtra("conversation_id", remoteMessage.getData().get("conversation_id"));

                intent.putExtra("user_id", remoteMessage.getData().get("user_id"));
                intent.putExtra("id",remoteMessage.getData().get("id"));

                intent.putExtra("created", remoteMessage.getData().get("created"));


                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            }else {

                Intent intent = new Intent(this, MainActivity.class);

                showNotification(remoteMessage, intent);
            }
            }
            }
    }

    private void showNotification(RemoteMessage message, Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("body"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.logo);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
